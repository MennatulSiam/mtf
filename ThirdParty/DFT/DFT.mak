DFT_ROOT_DIR = ThirdParty/DFT
DFT_SRC_DIR = ${DFT_ROOT_DIR}/src
DFT_INCLUDE_DIR = ${DFT_ROOT_DIR}/include
DFT_HEADER_DIR = ${DFT_INCLUDE_DIR}/mtf/${DFT_ROOT_DIR}
ISMAR_DEMO_DIR=${DFT_ROOT_DIR}/ISMAR_Demo

THIRD_PARTY_TRACKERS += DFT
_THIRD_PARTY_TRACKERS_SO += HomographyEstimation 
DFT_HEADERS = $(addprefix  ${DFT_HEADER_DIR}/, DFT.h)

DFT_LIB_MODULES = Utilities Homography IterativeOptimization  
DFT_LIB_INCLUDES = HomographyEstimation Typedefs
DFT_LIB_HEADERS = $(addprefix ${DFT_HEADER_DIR}/,$(addsuffix .hpp, ${DFT_LIB_MODULES} ${DFT_LIB_INCLUDES}))
DFT_LIB_SRC = $(addprefix ${DFT_SRC_DIR}/,$(addsuffix .cpp, ${DFT_LIB_MODULES}))

THIRD_PARTY_HEADERS += ${DFT_HEADERS} ${DFT_LIB_HEADERS} 
THIRD_PARTY_INCLUDE_DIRS += ${DFT_INCLUDE_DIR}

${BUILD_DIR}/DFT.o: ${DFT_SRC_DIR}/DFT.cc ${DFT_HEADERS} ${ROOT_HEADER_DIR}/TrackerBase.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${CT_FLAGS} $< ${FLAGS64} ${FLAGSCV} -I${DFT_INCLUDE_DIR} -I${ROOT_INCLUDE_DIR} -o $@
	
${MTF_LIB_INSTALL_DIR}/libHomographyEstimation.so: ${DFT_ROOT_DIR}/libHomographyEstimation.so
	sudo cp -f $< $@
${DFT_ROOT_DIR}/libHomographyEstimation.so: ${DFT_LIB_SRC} ${DFT_LIB_HEADERS} ${ISMAR_DEMO_DIR}/main.cpp
	$(MAKE) all -C ${DFT_ROOT_DIR} --no-print-directory
