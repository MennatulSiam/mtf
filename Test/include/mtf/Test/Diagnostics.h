#ifndef MTF_DIAGNOSTICS_H
#define MTF_DIAGNOSTICS_H

#include "DiagBase.h"

#define DIAGNOSTICS_UPDATE_TYPE 0
#define DIAGNOSTICS_SHOW_PATCHES 0
#define DIAGNOSTICS_SHOW_CORNERS 0
#define DIAGNOSTICS_ENABLE_VALIDATION 0
#define DIAGNOSTICS_VALIDATION_PREC 1e-20

_MTF_BEGIN_NAMESPACE

struct DiagnosticsParams{

	enum class UpdateType { Additive, Compositional };

	UpdateType update_type; 
	bool show_corners;
	bool show_patches;
	bool enable_validation;
	double validation_prec;


	DiagnosticsParams(UpdateType _update_type, 
		bool _show_corners, bool _show_patches,
		bool _enable_validation, double _validation_prec){
		update_type = _update_type;
		show_corners = _show_corners;
		show_patches = _show_patches;
		enable_validation = _enable_validation;
		validation_prec = _validation_prec;

	}
	DiagnosticsParams(DiagnosticsParams *params = nullptr) :
		update_type(static_cast<UpdateType>(DIAGNOSTICS_UPDATE_TYPE)),
		show_corners(DIAGNOSTICS_SHOW_CORNERS),
		show_patches(DIAGNOSTICS_SHOW_PATCHES),
		enable_validation(DIAGNOSTICS_ENABLE_VALIDATION),
		validation_prec(DIAGNOSTICS_VALIDATION_PREC){
		if(params){
			update_type = params->update_type;
			show_corners = params->show_corners;
			show_patches = params->show_patches;
			enable_validation = params->enable_validation;
			validation_prec = params->validation_prec;
		}
	}
};

template<class AM, class SSM>
class Diagnostics : public DiagBase {

public:
	typedef DiagnosticsParams ParamType;
	ParamType params;
	typedef typename ParamType::UpdateType UpdateType;
	typedef DiagBase::AnalyticalDataType ADT;
	typedef DiagBase::NumericalDataType NDT;

	AM *am;
	SSM *ssm;

	typedef typename  AM::ParamType AMParams;
	typedef typename SSM::ParamType SSMParams;

	int frame_id;
	int n_pix;

	VectorXd inv_state;

	//! N x S jacobians of the pix values w.r.t the SSM state vector where N = resx * resy
	//! is the no. of pixels in the object patch
	MatrixXd init_pix_jacobian, init_pix_hessian;
	MatrixXd curr_pix_jacobian, curr_pix_hessian;
	MatrixXd mean_pix_jacobian, mean_pix_hessian;

	//! 1 x S Jacobian of the AM error norm w.r.t. SSM state vector
	RowVectorXd similarity_jacobian;
	//! S x S Hessian of the AM error norm w.r.t. SSM state vector
	MatrixXd hessian, init_hessian;
	MatrixXd init_self_hessian, init_self_hessian2;
	MatrixXd curr_self_hessian, curr_self_hessian2;

	VectorXd init_dist_vec, curr_dist_vec;

	Matrix3d warp_update;
	char *update_name;

	Diagnostics(ParamType *diag_params = nullptr,
		AMParams *am_params = nullptr, SSMParams *ssm_params = nullptr);
	virtual ~Diagnostics(){
		if(am){ delete(am); }
		if(ssm){ delete(ssm); }			
	}

	void setImage(const cv::Mat &img) override{ am->setCurrImg(img); }
	void initialize(const cv::Mat &corners) override;
	void update(const cv::Mat &corners) override;

	void generateAnalyticalData(VectorXd &param_range,
		int n_pts, ADT data_type, const char* fname = nullptr) override;
	void generateInverseAnalyticalData(VectorXd &param_range,
		int n_pts, ADT data_type, const char* fname = nullptr) override;

	void generateNumericalData(VectorXd &param_range_vec, int n_pts,
		NDT data_type, const char* fname, double grad_diff) override;
	void generateInverseNumericalData(VectorXd &param_range_vec,
		int n_pts, NDT data_type, const char* fname, double grad_diff) override;

	void generateSSMParamData(VectorXd &param_range_vec,
		int n_pts, const char* fname) override;

private:
	char *init_patch_win_name;
	char *curr_patch_win_name;

	char *init_img_win_name;
	char *curr_img_win_name;

	cv::Mat init_patch, init_patch_uchar;
	cv::Mat curr_patch, curr_patch_uchar;

	cv::Mat init_img_cv, init_img_cv_uchar;
	cv::Mat curr_img_cv, curr_img_cv_uchar;

	EigImgT init_img;

	cv::Mat init_corners;
	cv::Point2d curr_corners_cv[4];

	MatrixXd ssm_grad_norm;
	RowVectorXd ssm_grad_norm_mean;

	void updateSSM(VectorXd &state_update);
	void resetSSM(VectorXd &state_update);

	void initializePixJacobian();
	void updateInitPixJacobian();
	void updateCurrPixJacobian();

	void initializePixHessian();
	void updateInitPixHessian();
	void updateCurrPixHessian();
	
	void updateInitSimilarity();
	void updateInitGrad();
	void updateInitHess();
	void updateInitSelfHess();

	void updateCurrSimilarity();
	void updateCurrGrad();
	void updateCurrSelfHess();

	bool validateHessians(const MatrixXd &self_hessian);

	void drawCurrCorners(cv::Mat &img, int state_id=-1);
};
_MTF_END_NAMESPACE

#endif

