set(STATE_SPACE_MODELS ProjectiveBase LieHomography CornerHomography Homography SL3 Affine Similitude Isometry Transcaling Translation)
addPrefixAndSuffix("${STATE_SPACE_MODELS}" "SSM/src/" ".cc" STATE_SPACE_MODELS_SRC)
set(MTF_SRC ${MTF_SRC} ${STATE_SPACE_MODELS_SRC})
set(MTF_INCLUDE_DIRS ${MTF_INCLUDE_DIRS} SSM/include)
