#include "mtf/SSM/Affine.h"
#include "mtf/Utilities/warpUtils.h"
#include "mtf/Utilities/miscUtils.h"

_MTF_BEGIN_NAMESPACE

Affine::Affine(int resx, int resy,
AffineParams *params_in) : ProjectiveBase(resx, resy),
params(params_in){

	printf("\n");
	printf("initializing Affine SSM with:\n");
	printf("resx: %d\n", resx);
	printf("resy: %d\n", resy);
	printf("normalized_init: %d\n", params.normalized_init);
	printf("debug_mode: %d\n", params.debug_mode);

	name = "affine";
	state_size = 6;
	curr_state.resize(state_size);
}

void Affine::setCorners(const CornersT& corners){
	if(params.normalized_init){
		init_corners = getNormCorners();
		init_corners_hm = getHomNormCorners();
		init_pts = getNormPts();
		init_pts_hm = getHomNormPts();

		curr_warp = utils::computeAffineNDLT(init_corners, corners);
		getStateFromWarp(curr_state, curr_warp);

		curr_pts.noalias() = curr_warp.topRows<2>() * init_pts_hm;
		curr_corners.noalias() = curr_warp.topRows<2>() * init_corners_hm;

		utils::homogenize(curr_pts, curr_pts_hm);
		utils::homogenize(curr_corners, curr_corners_hm);
	} else {
		curr_corners = corners;
		utils::homogenize(curr_corners, curr_corners_hm);

		getPtsFromCorners(curr_warp, curr_pts, curr_pts_hm, curr_corners);

		init_corners = curr_corners;
		init_pts = curr_pts;
		utils::homogenize(init_corners, init_corners_hm);
		utils::homogenize(init_pts, init_pts_hm);

		curr_warp = Matrix3d::Identity();
		curr_state.fill(0);
	}
}

void Affine::compositionalUpdate(const VectorXd& state_update){
	VALIDATE_SSM_STATE(state_update);

	getWarpFromState(warp_update_mat, state_update);
	curr_warp = curr_warp * warp_update_mat;
	getStateFromWarp(curr_state, curr_warp);

	//curr_pts_hm.noalias() = curr_warp * init_pts_hm;
	//curr_corners_hm.noalias() = curr_warp * init_corners_hm;
	//utils::dehomogenize(curr_pts_hm, curr_pts);
	//utils::dehomogenize(curr_corners_hm, curr_corners);

	curr_pts.noalias() = curr_warp.topRows<2>() * init_pts_hm;
	curr_corners.noalias() = curr_warp.topRows<2>() * init_corners_hm;

	//utils::printMatrix(curr_warp, "curr_warp", "%15.9f");
	//utils::printMatrix(affine_warp_mat, "affine_warp_mat", "%15.9f");
}

void Affine::setState(const VectorXd &ssm_state){
	VALIDATE_SSM_STATE(ssm_state);
	curr_state = ssm_state;
	getWarpFromState(curr_warp, curr_state);
	curr_pts.noalias() = curr_warp.topRows<2>() * init_pts_hm;
	curr_corners.noalias() = curr_warp.topRows<2>() * init_corners_hm;
}

void Affine::getWarpFromState(Matrix3d &warp_mat,
	const VectorXd& ssm_state){
	VALIDATE_SSM_STATE(ssm_state);

	warp_mat(0, 0) = 1 + ssm_state(2);
	warp_mat(0, 1) = ssm_state(3);
	warp_mat(0, 2) = ssm_state(0);
	warp_mat(1, 0) = ssm_state(4);
	warp_mat(1, 1) = 1 + ssm_state(5);
	warp_mat(1, 2) = ssm_state(1);
	warp_mat(2, 0) = 0;
	warp_mat(2, 1) = 0;
	warp_mat(2, 2) = 1;
}

void Affine::getStateFromWarp(VectorXd &state_vec,
	const Matrix3d& warp_mat){
	VALIDATE_SSM_STATE(state_vec);
	VALIDATE_AFFINE_WARP(warp_mat);

	state_vec(0) = warp_mat(0, 2);
	state_vec(1) = warp_mat(1, 2);
	state_vec(2) = warp_mat(0, 0) - 1;
	state_vec(3) = warp_mat(0, 1);
	state_vec(4) = warp_mat(1, 0);
	state_vec(5) = warp_mat(1, 1) - 1;
}

void Affine::invertState(VectorXd& inv_state, const VectorXd& state){
	getWarpFromState(warp_mat, state);
	inv_warp_mat = warp_mat.inverse();
	inv_warp_mat /= inv_warp_mat(2, 2);
	getStateFromWarp(inv_state, inv_warp_mat);
}

void Affine::cmptInitPixJacobian(MatrixXd &jacobian_prod,
	const PixGradT &pix_jacobian){
	VALIDATE_SSM_JACOBIAN(jacobian_prod, pix_jacobian);

	for(int i = 0; i < n_pts; i++){
		double x = init_pts(0, i);
		double y = init_pts(1, i);
		double Ix = pix_jacobian(i, 0);
		double Iy = pix_jacobian(i, 1);

		jacobian_prod(i, 0) = Ix;
		jacobian_prod(i, 1) = Iy;
		jacobian_prod(i, 2) = Ix * x;
		jacobian_prod(i, 3) = Ix * y;
		jacobian_prod(i, 4) = Iy * x;
		jacobian_prod(i, 5) = Iy * y;
	}
}

void Affine::getInitPixGrad(Matrix2Xd &ssm_grad, int pix_id) {
	double x = init_pts(0, pix_id);
	double y = init_pts(1, pix_id);
	ssm_grad <<
		1, 0, x, y, 0, 0,
		0, 1, 0, 0, x, y;
}

void Affine::cmptInitPixHessian(MatrixXd &pix_hess_ssm, const PixHessT &pix_hess_coord,
	const PixGradT &pix_grad){
	VALIDATE_SSM_HESSIAN(pix_hess_ssm, pix_hess_coord, pix_grad);

	Matrix26d ssm_grad;
	for(int pt_id = 0; pt_id < n_pts; pt_id++){
		double x = init_pts(0, pt_id);
		double y = init_pts(1, pt_id);
		ssm_grad <<
			1, 0, x, y, 0, 0,
			0, 1, 0, 0, x, y;
		Map<Matrix6d> curr_pix_hess_ssm((double*)pix_hess_ssm.col(pt_id).data());
		curr_pix_hess_ssm = ssm_grad.transpose()*Map<Matrix2d>((double*)pix_hess_coord.col(pt_id).data())*ssm_grad;
	}
}

void Affine::cmptApproxPixJacobian(MatrixXd &jacobian_prod,
	const PixGradT &pix_jacobian) {
	VALIDATE_SSM_JACOBIAN(jacobian_prod, pix_jacobian);
	double a = curr_state(2) + 1, b = curr_state(3);
	double c = curr_state(4), d = curr_state(5) + 1;
	double inv_det = 1.0 / (a*d - b*c);

	//utils::printScalar(inv_det, "inv_det");

	for(int i = 0; i < n_pts; i++){
		double x = init_pts(0, i);
		double y = init_pts(1, i);
		double Ix = pix_jacobian(i, 0);
		double Iy = pix_jacobian(i, 1);
		double Ixx = Ix * x;
		double Ixy = Ix * y;
		double Iyy = Iy * y;
		double Iyx = Iy * x;

		jacobian_prod(i, 0) = (Ix*d - Iy*c) * inv_det;
		jacobian_prod(i, 1) = (Iy*a - Ix*b) * inv_det;
		jacobian_prod(i, 2) = (Ixx*d - Iyx*c) * inv_det;
		jacobian_prod(i, 3) = (Ixy*d - Iyy*c) * inv_det;
		jacobian_prod(i, 4) = (Iyx*a - Ixx*b) * inv_det;
		jacobian_prod(i, 5) = (Iyy*a - Ixy*b) * inv_det;
	}
}

void Affine::updateGradPts(double grad_eps){
	Vector2d diff_vec_x_warped = curr_warp.topRows<2>().col(0) * grad_eps;
	Vector2d diff_vec_y_warped = curr_warp.topRows<2>().col(1) * grad_eps;

	for(int pix_id = 0; pix_id < n_pts; pix_id++){
		grad_pts(0, pix_id) = curr_pts(0, pix_id) + diff_vec_x_warped(0);
		grad_pts(1, pix_id) = curr_pts(1, pix_id) + diff_vec_x_warped(1);

		grad_pts(2, pix_id) = curr_pts(0, pix_id) - diff_vec_x_warped(0);
		grad_pts(3, pix_id) = curr_pts(1, pix_id) - diff_vec_x_warped(1);

		grad_pts(4, pix_id) = curr_pts(0, pix_id) + diff_vec_y_warped(0);
		grad_pts(5, pix_id) = curr_pts(1, pix_id) + diff_vec_y_warped(1);

		grad_pts(6, pix_id) = curr_pts(0, pix_id) - diff_vec_y_warped(0);
		grad_pts(7, pix_id) = curr_pts(1, pix_id) - diff_vec_y_warped(1);
	}
}


void Affine::updateHessPts(double hess_eps){
	double hess_eps2 = 2 * hess_eps;

	Vector2d diff_vec_xx_warped = curr_warp.topRows<2>().col(0) * hess_eps2;
	Vector2d diff_vec_yy_warped = curr_warp.topRows<2>().col(1) * hess_eps2;
	Vector2d diff_vec_xy_warped = (curr_warp.topRows<2>().col(0) + curr_warp.topRows<2>().col(1)) * hess_eps;
	Vector2d diff_vec_yx_warped = (curr_warp.topRows<2>().col(0) - curr_warp.topRows<2>().col(1)) * hess_eps;

	for(int pix_id = 0; pix_id < n_pts; pix_id++){

		hess_pts(0, pix_id) = curr_pts(0, pix_id) + diff_vec_xx_warped(0);
		hess_pts(1, pix_id) = curr_pts(1, pix_id) + diff_vec_xx_warped(1);

		hess_pts(2, pix_id) = curr_pts(0, pix_id) - diff_vec_xx_warped(0);
		hess_pts(3, pix_id) = curr_pts(1, pix_id) - diff_vec_xx_warped(1);

		hess_pts(4, pix_id) = curr_pts(0, pix_id) + diff_vec_yy_warped(0);
		hess_pts(5, pix_id) = curr_pts(1, pix_id) + diff_vec_yy_warped(1);

		hess_pts(6, pix_id) = curr_pts(0, pix_id) - diff_vec_yy_warped(0);
		hess_pts(7, pix_id) = curr_pts(1, pix_id) - diff_vec_yy_warped(1);

		hess_pts(8, pix_id) = curr_pts(0, pix_id) + diff_vec_xy_warped(0);
		hess_pts(9, pix_id) = curr_pts(1, pix_id) + diff_vec_xy_warped(1);

		hess_pts(10, pix_id) = curr_pts(0, pix_id) - diff_vec_xy_warped(0);
		hess_pts(11, pix_id) = curr_pts(1, pix_id) - diff_vec_xy_warped(1);

		hess_pts(12, pix_id) = curr_pts(0, pix_id) + diff_vec_yx_warped(0);
		hess_pts(13, pix_id) = curr_pts(1, pix_id) + diff_vec_yx_warped(1);

		hess_pts(14, pix_id) = curr_pts(0, pix_id) - diff_vec_yx_warped(0);
		hess_pts(15, pix_id) = curr_pts(1, pix_id) - diff_vec_yx_warped(1);
	}
}

void Affine::estimateWarpFromCorners(VectorXd &state_update, const Matrix24d &in_corners,
	const Matrix24d &out_corners){
	VALIDATE_SSM_STATE(state_update);
	Matrix3d warp_update_mat = utils::computeAffineDLT(in_corners, out_corners);
	getStateFromWarp(state_update, warp_update_mat);
}

void Affine::applyWarpToCorners(Matrix24d &warped_corners, const Matrix24d &orig_corners,
	const VectorXd &ssm_state){
	getWarpFromState(warp_mat, ssm_state);
	for(int i = 0; i < 4; i++){
		warped_corners(0, i) = warp_mat(0, 0)*orig_corners(0, i) + warp_mat(0, 1)*orig_corners(1, i) +
			warp_mat(0, 2);
		warped_corners(1, i) = warp_mat(1, 0)*orig_corners(0, i) + warp_mat(1, 1)*orig_corners(1, i) +
			warp_mat(1, 2);
	}
}

void Affine::applyWarpToPts(Matrix2Xd &warped_pts, const Matrix2Xd &orig_pts,
	const VectorXd &ssm_state){
	getWarpFromState(warp_mat, ssm_state);
	int n_pts = orig_pts.cols();
	for(int i = 0; i < n_pts; i++){
		warped_pts(0, i) = warp_mat(0, 0)*orig_pts(0, i) + warp_mat(0, 1)*orig_pts(1, i) +
			warp_mat(0, 2);
		warped_pts(1, i) = warp_mat(1, 0)*orig_pts(0, i) + warp_mat(1, 1)*orig_pts(1, i) +
			warp_mat(1, 2);
	}
}

_MTF_END_NAMESPACE

