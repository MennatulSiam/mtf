#ifndef PRE_PROC_H
#define PRE_PROC_H

#include "opencv2/imgproc/imgproc.hpp"

// some basic functions for preprocessing the raw input image before using them for tracking
struct PreProc{
	cv::Mat frame_rgb, frame_gs;
	PreProc(){}
	virtual ~PreProc(){}
	virtual void initialize(const cv::Mat &frame_raw){
		frame_rgb.create(frame_raw.rows, frame_raw.cols, CV_32FC3);
		frame_gs.create(frame_raw.rows, frame_raw.cols, CV_32FC1);
		update(frame_raw);
	}
	virtual void update(const cv::Mat &frame_raw){
		frame_raw.convertTo(frame_rgb, frame_rgb.type());
		cv::cvtColor(frame_rgb, frame_gs, CV_BGR2GRAY);
		apply(frame_gs);
	}
	virtual void apply(cv::Mat &img_gs) const = 0;
	virtual const cv::Mat& getFrame(){
		return frame_gs;		
	}

};

struct GaussianSmoothing : public PreProc{
	cv::Size kernel_size;
	double sigma_x;
	double sigma_y;
	GaussianSmoothing(
		int _kernel_size = 5, 
		double _sigma_x = 3.0, 
		double _sigma_y = 0) :
		PreProc(),
		kernel_size (_kernel_size, _kernel_size),
		sigma_x(_sigma_x),
		sigma_y(_sigma_y){
		printf("Initializing Gaussian Smoothing with:\n");
		printf("kernel_size: %d, %d\n", kernel_size.width, kernel_size.height);
		printf("sigma_x: %f\n", sigma_x);
		printf("sigma_y: %f\n", sigma_y);

	}
	void apply(cv::Mat &img_gs) const override{
		cv::GaussianBlur(img_gs, img_gs, kernel_size, sigma_x, sigma_y);
	}
};
struct MedianFiltering : public PreProc{
	int kernel_size;
	MedianFiltering(int _kernel_size = 5) :
		PreProc(), kernel_size(_kernel_size){
		printf("Initializing Median Filtering with:\n");
		printf("kernel_size: %d\n", kernel_size);
	}
	void apply(cv::Mat &img_gs) const override{
		cv::medianBlur(img_gs, img_gs, kernel_size);
	}
};
struct NormalizedBoxFltering : public PreProc{
	cv::Size kernel_size;
	NormalizedBoxFltering(int _kernel_size = 5) :
		PreProc(), kernel_size(_kernel_size, _kernel_size){
		printf("Initializing Normalized Box Fltering with:\n");
		printf("kernel_size: %d, %d\n", kernel_size.width, kernel_size.height);
	}
	void apply(cv::Mat &img_gs) const override{
		cv::blur(img_gs, img_gs, kernel_size);
	}
};
struct BilateralFiltering : public PreProc{
	int diameter;
	double sigma_col;
	double sigma_space;
	BilateralFiltering(
		int _diameter = 5,
		double _sigma_col = 15,
		double _sigma_space = 15) :
		PreProc(), 	diameter(_diameter), 
		sigma_col(_sigma_col), sigma_space(_sigma_space){
		printf("Initializing Bilateral Filtering with:\n");
		printf("diameter: %d\n", diameter);
		printf("sigma_col: %f\n", sigma_col);
		printf("sigma_space: %f\n", sigma_space);
	}
	void apply(cv::Mat &img_gs) const override{
		// OpenCV bilateral filterig does not work in place so a copy has to be made
		cv::Mat orig_img = img_gs.clone();
		//img_gs.copyTo(orig_img);
		cv::bilateralFilter(orig_img, img_gs, diameter, sigma_col, sigma_space);
	}
};
struct NoProcessing : public PreProc{
	NoProcessing() : PreProc(){}
	void apply(cv::Mat &img_gs) const override{}
};

#endif