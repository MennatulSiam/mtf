#include "mtf/mtf.h"

#include "mtf/Tools/cvUtils.h"
#include "mtf/Tools/inputCV.h"
#ifndef DISABLE_XVISION
#include "mtf/Tools/inputXV.h"
#endif
// classes for preprocessing the image
#include "mtf/Tools/PreProc.h"

#include "mtf/Config/parameters.h"
#include "mtf/Config/datasets.h"

#ifdef _WIN32
#define snprintf  _snprintf
#include <time.h>
#else
#include <ctime> 
#endif
#include <string.h>
#include <numeric>
#include <vector>
#include <iomanip>

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"

#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/path.hpp"

#define MAX_FPS 1e6

#ifdef _WIN32
#define start_input_timer() \
	clock_t start_time_with_input = clock()
#define start_tracking_timer() \
	clock_t start_time = clock()
#define end_both_timers(fps, fps_win) \
	clock_t end_time = clock();\
	fps = CLOCKS_PER_SEC / static_cast<double>(end_time - start_time);\
	fps_win = CLOCKS_PER_SEC / static_cast<double>(end_time - start_time_with_input)
#else
#define start_input_timer() \
	timespec start_time_with_input;\
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start_time_with_input)
#define start_tracking_timer() \
	timespec start_time;\
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start_time)
#define end_both_timers(fps, fps_win) \
	timespec end_time;\
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &end_time);\
	fps = 1.0 / ((double)(end_time.tv_sec - start_time.tv_sec) +\
		1e-9*(double)(end_time.tv_nsec - start_time.tv_nsec));\
	fps_win = 1.0 / ((double)(end_time.tv_sec - start_time_with_input.tv_sec) +\
		1e-9*(double)(end_time.tv_nsec - start_time_with_input.tv_nsec))
#endif

#ifndef DISABLE_XVISION
#define XVISION_PIPELINE 'x'
#endif
#define OPENCV_PIPELINE 'c'

using namespace std;
using namespace mtf::params;
namespace fs = boost::filesystem;

int main(int argc, char * argv[]) {
	printf("Starting MTF...\n");
	// check if a custom configuration directory has been specified
	if(argc > 2 && !strcmp(argv[1], "config_dir")){
		config_dir = string(argv[2]);
		printf("Reading configuration files from: %s\n", config_dir.c_str());
	}
	vector<char*> fargv;
	// read general parameters
	int fargc = readParams(fargv, (config_dir + "/mtf.cfg").c_str());
	if(fargc){
		parseArgumentPairs(fargv.data(), fargc);
		fargv.clear();
	}
	// read parameters specific to different modules
	fargc = readParams(fargv, (config_dir + "/modules.cfg").c_str());
	if(fargc){
		parseArgumentPairs(fargv.data(), fargc);
		fargv.clear();
	}
	// parse command line arguments
	parseArgumentPairs(argv, argc, 1, 1);
	if(actor_id >= 0){
		int n_actors = sizeof(actors) / sizeof(actors[0]);
		printf("n_actors: %d\n", n_actors);
		if(actor_id >= n_actors){
			printf("Invalid actor id specified: %d\n", actor_id);
			return 0;
		}
		actor = actors[actor_id];
		if(source_id >= 0){
			source_name = combined_sources[actor_id][source_id];
		}
	}
#ifdef ENABLE_PARALLEL
	Eigen::initParallel();
#endif
	printf("*******************************\n");
	printf("Using parameters:\n");
	printf("n_trackers: %d\n", n_trackers);
	printf("actor_id: %d\n", actor_id);
	printf("source_id: %d\n", source_id);
	printf("source_name: %s\n", source_name.c_str());	
	printf("actor: %s\n", actor.c_str());
	printf("pipeline: %c\n", pipeline);
	printf("img_source: %c\n", img_source);
	printf("show_cv_window: %d\n", show_cv_window);
	printf("*******************************\n");

	InputBase *input;
	/* initialize pipeline*/
	if(pipeline == OPENCV_PIPELINE) {
		input = new InputCV(img_source, source_name, source_fmt, source_path, buffer_count);
	}
#ifndef DISABLE_XVISION
	else if(pipeline == XVISION_PIPELINE) {
		input = new InputXV(img_source, source_name, source_fmt, source_path, buffer_count);
	}
#endif

	else {
		printf("Invalid video pipeline provided: %c\n", pipeline);
		return 1;
	}
	if(!input->initialize()){
		printf("Pipeline could not be initialized successfully. Exiting...\n");
		return 1;
	}
	printf("n_frames=%d\n", input->n_frames);
	printf("source_path: %s\n", source_path.c_str());

	CVUtils cv_utils;
	obj_struct* init_object = cv_utils.readObjectFromGT(source_name, source_path, input->n_frames,
		init_frame_id, debug_mode, use_opt_gt, opt_gt_ssm);

	//cv::Mat img_rgb, img_gs;
	//char img_fname[500];
	//snprintf(img_fname, 500, "%s/%s/frame00001.jpg", source_path, source_name);
	//cv::Mat img_raw = cv::imread(img_fname);
	//mtf::initializeFrame(img_rgb, img_gs, img_raw);	


	mtf::ImgParams img_params(resx, resy);
	mtf::SSD patch_extractor_am(&img_params);
	mtf::Translation patch_extractor_ssm(resx, resy);

	patch_extractor_ssm.initialize(cv_utils.ground_truth[0]);
	patch_extractor_am.initializePixVals(patch_extractor_ssm.getPts());

	int patch_size = patch_extractor_am.getPixCount();
	int n_extracted_patches = extracted_frame_ids.size();
	if (extracted_frame_ids[0] == -1) {
		n_extracted_patches = input->n_frames;
	}

	// Define matrix for extracted patches
	MatrixXd extracted_patches(patch_size, n_extracted_patches);
	NoProcessing no_proc;

	for(int patch_id = 0; patch_id < n_extracted_patches; patch_id++){
		int frame_id;
		if (extracted_frame_ids[0] == -1) {
			frame_id = patch_id + 1;
		}
		else frame_id = extracted_frame_ids[patch_id];

		char img_fname[500];
		snprintf(img_fname, 500, "%s/%s/frame%05d.jpg", source_path.c_str(), source_name.c_str(), frame_id);
		printf("Reading image: %s\n", img_fname);
		cv::Mat img_raw = cv::imread(img_fname);
		// Allocate memory for rgb and gray scale of type CV_32F 
		// and convert the given RGB image to grayscale
		no_proc.initialize(img_raw);
		// Share memory with eigen image
		patch_extractor_am.setCurrImg(no_proc.getFrame());
		patch_extractor_ssm.setCorners(cv_utils.ground_truth[frame_id-1]);
		patch_extractor_am.updatePixVals(patch_extractor_ssm.getPts());
		// **********DEBUG ******************
		//cout << "Using corners:\n";
		//cout << cv_utils.ground_truth[frame_id - 1]<<"\n";
		//cout << "SSM corners:\n";
		//cout << patch_extractor_ssm.getCorners() << "\n";
		//cout << "SSM points:\n";
		//cout << patch_extractor_ssm.getPts() << "\n";
		//cout << "Pixel Values:\n";
		//cout << patch_extractor_am.getCurrPixVals() << "\n";

		// Save the patch into a column of the output matrix
		extracted_patches.col(patch_id) = patch_extractor_am.getCurrPixVals();
		// Debug, only show one image at every 50
		if (patch_id % 50 == 0) {
			// Construct Mat object for current patch. It points to the memory without copying data
			cv::Mat curr_img_cv = cv::Mat(patch_extractor_am.getResY(), patch_extractor_am.getResX(), CV_64FC1,
				const_cast<double*>(patch_extractor_am.getCurrPixVals().data()));
			// Construct Mat object for the uint8 gray scale patch
			cv::Mat  curr_img_cv_uchar(patch_extractor_am.getResY(), patch_extractor_am.getResX(), CV_8UC1);
			// Convert the RGB patch to grayscale
			curr_img_cv.convertTo(curr_img_cv_uchar, curr_img_cv_uchar.type());
			cv::Mat  img_gs_uchar(no_proc.getFrame().rows, no_proc.getFrame().cols, CV_8UC1);
			no_proc.getFrame().convertTo(img_gs_uchar, img_gs_uchar.type());

			imshow("Image", img_raw);
			imshow("Image GS", img_gs_uchar);
			imshow("Patch", curr_img_cv_uchar);
			if(cv::waitKey(0)%256 == 27)
				break;
		}
	}

	string out_fname = cv::format("%s/%s/%s_%dx%d_%d.bin",
		root_path.c_str(), actor.c_str(), source_name.c_str(), 
		resx, resy, extraction_id);
	cout << "Saving to file: " << out_fname << endl;
	ofstream out_file;
	out_file.open(out_fname, ios::out | ios::binary);
	out_file.write((char*)(&patch_size),sizeof(int));// rows
	out_file.write((char*)(&n_extracted_patches),sizeof(int));// cols
	out_file.write((char*)(extracted_patches.data()),
		sizeof(double)*extracted_patches.size());
	out_file.close();
	return 0;
}
