// tools for capturing images from disk or cameras
#include "mtf/Tools/inputCV.h"
#ifndef DISABLE_XVISION
#include "mtf/Tools/inputXV.h"
#endif
#ifndef DISABLE_VISP
#include "mtf/Tools/inputVP.h"
#endif
// general OpenCV utilities for selecting objects, reading ground truth, etc.
#include "mtf/Tools/cvUtils.h"
// parameters for different modules
#include "mtf/Config/parameters.h"

#ifdef _WIN32
#define snprintf  _snprintf
#include <time.h>
#else
#include <ctime> 
#endif
#include <string.h>
#include <numeric>
#include <vector>
#include <iomanip>

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"

#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/path.hpp"

#define MAX_FPS 1e6

#ifdef _WIN32
#define start_input_timer() \
	clock_t start_time = clock()
#define end_input_timer(fps_win) \
	clock_t end_time = clock();\
	fps_win = CLOCKS_PER_SEC / static_cast<double>(end_time - start_time)
#else
#define start_input_timer() \
	timespec start_time;\
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start_time)
#define end_input_timer(fps_win) \
	timespec end_time;\
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &end_time);\
	fps_win = 1.0 / ((double)(end_time.tv_sec - start_time_with_input.tv_sec) +\
		1e-9*(double)(end_time.tv_nsec - start_time_with_input.tv_nsec))
#endif

#define OPENCV_PIPELINE 'c'
#ifndef DISABLE_XVISION
#define XVISION_PIPELINE 'x'
#endif
#ifndef DISABLE_VISP
#define VISP_PIPELINE 'v'
#endif
using namespace std;
using namespace mtf::params;
namespace fs = boost::filesystem;

int main(int argc, char * argv[]) {
	// *************************************************************************************************** //
	// ********************************** read configuration parameters ********************************** //
	// *************************************************************************************************** //

	// check if a custom configuration directory has been specified
	if(argc > 2 && !strcmp(argv[1], "config_dir")){
		config_dir = string(argv[2]);
		printf("Reading configuration files from: %s\n", config_dir.c_str());
	}
	vector<char*> fargv;
	// read general parameters
	int fargc = readParams(fargv, (config_dir + "/mtf.cfg").c_str());
	if(fargc){
		if(!parseArgumentPairs(fargv.data(), fargc)){
			printf("Error in parsing mtf.cfg\n");
			return EXIT_FAILURE;
		}
		fargv.clear();
	}
	// read parameters specific to different modules
	fargc = readParams(fargv, (config_dir + "/modules.cfg").c_str());
	if(fargc){
		if(!parseArgumentPairs(fargv.data(), fargc)){
			printf("Error in parsing modules.cfg\n");
			return EXIT_FAILURE;
		}
		fargv.clear();
	}
	// parse command line arguments
	if(!parseArgumentPairs(argv, argc, 1, 1)){
		printf("Error in parsing command line arguments\n");
		return EXIT_FAILURE;
	}
	printf("*******************************\n");
	printf("Using parameters:\n");
	printf("actor_id: %d\n", actor_id);
	printf("source_id: %d\n", source_id);
	printf("source_name: %s\n", source_name.c_str());
	printf("actor: %s\n", actor.c_str());
	printf("pipeline: %c\n", pipeline);
	printf("img_source: %c\n", img_source);
	printf("*******************************\n");

	// *********************************************************************************************** //
	// ********************************** initialize input pipeline ********************************** //
	// *********************************************************************************************** //

	InputBase *input = nullptr;
	if(pipeline == OPENCV_PIPELINE) {
		input = new InputCV(img_source, source_name, source_fmt, source_path, buffer_count);
	}
#ifndef DISABLE_XVISION
	else if(pipeline == XVISION_PIPELINE) {
		input = new InputXV(img_source, source_name, source_fmt, source_path, buffer_count);
	}
#endif
#ifndef DISABLE_VISP
	else if(pipeline == VISP_PIPELINE) {
		input = new InputVP(
			img_source, source_name, source_fmt, source_path, buffer_count, visp_usb_n_buffers,
			static_cast<VpResUSB>(visp_usb_res), static_cast<VpFpsUSB>(visp_usb_fps),
			static_cast<VpResFW>(visp_fw_res), static_cast<VpFpsFW>(visp_fw_fps)
			);
	}
#endif
	else {
		printf("Invalid video pipeline provided: %c\n", pipeline);
		return EXIT_FAILURE;
	}
	if(!input->initialize()){
		printf("Pipeline could not be initialized successfully. Exiting...\n");
		return EXIT_FAILURE;
	}

	//printf("done getting no. of frames\n");
	printf("n_frames=%d\n", input->n_frames);

	if(init_frame_id > 0){
		printf("Skipping %d frames...\n", init_frame_id);
	}
	for(int frame_id = 0; frame_id < init_frame_id; frame_id++){
		if(!input->update()){
			printf("Frame %d could not be read from the input pipeline", input->getFameID() + 1);
			return EXIT_FAILURE;
		}
	}
	// ************************************************************************************* //
	// ********************************* read ground truth ********************************* //
	// ************************************************************************************* //

	CVUtils cv_utils;
	if(!cv_utils.readGT(source_name, source_path, input->n_frames,
		init_frame_id, debug_mode)){
		printf("Ground truth could not be read.\n");
		return EXIT_FAILURE;
	}

	if(reinit_from_gt){
		if(!cv_utils.readReinitGT(source_name, source_path, input->n_frames,
			debug_mode, use_opt_gt, read_from_bin, opt_gt_ssm)){
			printf("Reinitialization ground truth could not be read.\n");
			return EXIT_FAILURE;
		}
	} 

	// ******************************************************************************************** //
	// *************************************** setup output *************************************** //
	// ******************************************************************************************** //

	string cv_win_name = "showGroundTruth :: " + source_name;
	if(show_cv_window) {
		cv::namedWindow(cv_win_name, cv::WINDOW_AUTOSIZE);
	}
	int valid_gt_frames = cv_utils.ground_truth.size();
	if(input->n_frames <= 0 || valid_gt_frames < input->n_frames){
		if(input->n_frames <= 0){
			printf("Ground truth is unavailable\n");
		} else if(valid_gt_frames > 0){
			printf("Ground truth is only available for %d out of %d frames\n",
				valid_gt_frames, input->n_frames);			
		} 
		return EXIT_FAILURE;
	}
	cv::VideoWriter output;
	if(record_frames){
		output.open("ground_truth.avi", CV_FOURCC('M', 'J', 'P', 'G'), 24, input->getFrame().size());
	}
	cv::Scalar gt_color(0, 255, 0), reinit_gt_color(0, 0, 255);
	cv::Point2d corners[4];

	// *********************************************************************************************** //
	// ************************************** start visualization ************************************ //
	// *********************************************************************************************** //
	cv::Point text_origin(10, 20);
	double text_font_size = 0.50;
	cv::Scalar text_color(0, 255, 0);

	while(true) {
		// *************************** display/save ground truth annotated frames *************************** //

		cv::Mat gt_corners = cv_utils.ground_truth[input->getFameID()];
		cv_utils.cornersToPoint2D(corners, gt_corners);
		line(input->getFrameMutable(), corners[0], corners[1], gt_color, line_thickness);
		line(input->getFrameMutable(), corners[1], corners[2], gt_color, line_thickness);
		line(input->getFrameMutable(), corners[2], corners[3], gt_color, line_thickness);
		line(input->getFrameMutable(), corners[3], corners[0], gt_color, line_thickness);

		if(reinit_from_gt){
			cv::Mat reinit_gt_corners = cv_utils.reinit_ground_truth[init_frame_id][input->getFameID() - init_frame_id];
			cv_utils.cornersToPoint2D(corners, reinit_gt_corners);
			line(input->getFrameMutable(), corners[0], corners[1], reinit_gt_color, line_thickness);
			line(input->getFrameMutable(), corners[1], corners[2], reinit_gt_color, line_thickness);
			line(input->getFrameMutable(), corners[2], corners[3], reinit_gt_color, line_thickness);
			line(input->getFrameMutable(), corners[3], corners[0], reinit_gt_color, line_thickness);
			//printf("frame_id: %d : \n", input->getFameID());
			//cout << reinit_gt_corners << "\n";
		}
		putText(input->getFrameMutable(), cv::format("frame: %d", input->getFameID() + 1), 
			text_origin, cv::FONT_HERSHEY_SIMPLEX, text_font_size, text_color);
		if(record_frames){
			output.write(input->getFrame());
		}
		imshow(cv_win_name, input->getFrame());
		int pressed_key = cv::waitKey(1 - pause_after_frame);
		if(pressed_key == 27){
			break;
		}
		if(pressed_key == 32){
			pause_after_frame = 1 - pause_after_frame;
		}
		if(input->n_frames > 0 && input->getFameID() >= input->n_frames - 1){
			printf("==========End of input stream reached==========\n");
			break;
		}

		// ******************************* update pipeline ******************************* //

		start_input_timer();
		// update pipeline
		if(!input->update()){
			printf("Frame %d could not be read from the input pipeline", input->getFameID() + 1);
			break;
		}
	}
	cv::destroyAllWindows();
	if(record_frames){
		output.release();
	}
	delete(input);
	return EXIT_SUCCESS;
}
