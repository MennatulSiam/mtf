#ifndef ZNCC_H
#define ZNCC_H

#include "SSDBase.h"

_MTF_BEGIN_NAMESPACE

// Zero mean Normalized Cross Correlation
class ZNCC : public SSDBase{
public:

	typedef ImgParams ParamType; 

	//! mean, variance and standard deviation of the initial pixel values
	double init_pix_mean, init_pix_var, init_pix_std;
	//! mean, variance and standard deviation of the current pixel values
	double curr_pix_mean, curr_pix_var, curr_pix_std;

	ZNCC(ParamType *ncc_params);
	ZNCC() : SSDBase(){
		init_pix_mean = init_pix_var = init_pix_std = 0;
		curr_pix_mean = curr_pix_var = curr_pix_std = 0;
	}
	void initializePixVals(const Matrix2Xd& curr_pts) override;
	void updatePixVals(const Matrix2Xd& curr_pts) override;
};

_MTF_END_NAMESPACE

#endif