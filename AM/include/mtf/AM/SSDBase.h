#ifndef SSD_BASE_H
#define SSD_BASE_H

#include "AppearanceModel.h"

_MTF_BEGIN_NAMESPACE

//! base class for all appearance models that use the negative L2 norm of the difference between
//! the initial and current pixel values (original or modified) as the appearance model
class SSDBase : public AppearanceModel{
public:
	VectorXdM curr_pix_diff;

	SSDBase(ImgParams *img_params = nullptr);
	bool supportsSPI() override{ return true; }
	void initialize() override;
	void initializeGrad() override;
	void initializeHess() override{}

	double getLikelihood() override{
		// since SSD can be numerically too large for exponential function to work,
		// we take the square root of the per pixel SSD instead
		return exp(-sqrt(-similarity/static_cast<double>(n_pix)));
	}

	//-----------------------------------------------------------------------------------//
	//-------------------------------update functions------------------------------------//
	//-----------------------------------------------------------------------------------//

	//void update(bool prereq_only = true) override;
	//void cmptInitJacobian(RowVectorXd &init_jacobian,
	//	const MatrixXd &init_pix_jacobian) override;
	//void cmptCurrJacobian(RowVectorXd &curr_jacobian,
	//	const MatrixXd &curr_pix_jacobian) override;
	//void cmptDifferenceOfJacobians(RowVectorXd &diff_of_jacobians,
	//	const MatrixXd &init_pix_jacobian, 
	//	const MatrixXd &curr_pix_jacobian) override;
	//void cmptInitHessian(MatrixXd &init_hessian,
	//	const MatrixXd &init_pix_jacobian) override;
	//void cmptCurrHessian(MatrixXd &curr_hessian,
	//	const MatrixXd &curr_pix_jacobian) override;

	void update(bool prereq_only=true) override{
		curr_pix_diff = curr_pix_vals - init_pix_vals;
		if(prereq_only){ return; }
#ifndef DISABLE_SPI
		if(spi_mask){
			VectorXd masked_err_vec = VectorXbM((bool*)spi_mask, n_pix).select(curr_pix_diff, 0);
			similarity = -masked_err_vec.squaredNorm() / 2;
		} else{
#endif
			similarity = -curr_pix_diff.squaredNorm() / 2;
#ifndef DISABLE_SPI
		}
#endif
	}

	// nothing is done here since init_grad is same as and shares memory
	// with curr_pix_diff that is updated in update
	void updateInitGrad() override{}
	// curr_grad is same as negative of init_grad
	void updateCurrGrad() override{ curr_grad = -init_grad; }

	void cmptInitJacobian(RowVectorXd &init_jacobian,
		const MatrixXd &init_pix_jacobian) override{
#ifndef DISABLE_SPI
		if(spi_mask){
			getJacobian(init_jacobian, spi_mask, init_grad, init_pix_jacobian);
		} else{
#endif
			init_jacobian.noalias() = init_grad * init_pix_jacobian;
#ifndef DISABLE_SPI
		}
#endif
	}
	void cmptCurrJacobian(RowVectorXd &curr_jacobian,
		const MatrixXd &curr_pix_jacobian) override{
#ifndef DISABLE_SPI
		if(spi_mask){
			getJacobian(curr_jacobian, spi_mask, curr_grad, curr_pix_jacobian);
		} else{
#endif
			curr_jacobian.noalias() = curr_grad * curr_pix_jacobian;
#ifndef DISABLE_SPI
		}
#endif
	}
	void cmptDifferenceOfJacobians(RowVectorXd &diff_of_jacobians,
		const MatrixXd &init_pix_jacobian, const MatrixXd &curr_pix_jacobian) override{
#ifndef DISABLE_SPI
		if(spi_mask){
			getDifferenceOfJacobians(diff_of_jacobians, spi_mask, init_pix_jacobian, curr_pix_jacobian);
		} else{
#endif
			diff_of_jacobians.noalias() = curr_grad * (init_pix_jacobian + curr_pix_jacobian);
#ifndef DISABLE_SPI
		}
#endif
	}

	void cmptInitHessian(MatrixXd &init_hessian,
		const MatrixXd &init_pix_jacobian) override{
#ifndef DISABLE_SPI
		if(spi_mask){
			getHessian(init_hessian, spi_mask, init_pix_jacobian);
		} else{
#endif
			init_hessian.noalias() = -init_pix_jacobian.transpose() * init_pix_jacobian;
#ifndef DISABLE_SPI
		}
#endif
	}

	void cmptCurrHessian(MatrixXd &curr_hessian,
		const MatrixXd &curr_pix_jacobian) override{
#ifndef DISABLE_SPI
		if(spi_mask){
			getHessian(curr_hessian, spi_mask, curr_pix_jacobian);
		} else{
#endif
			curr_hessian.noalias() = -curr_pix_jacobian.transpose() * curr_pix_jacobian;
#ifndef DISABLE_SPI
		}
#endif
	}
	void cmptInitHessian(MatrixXd &init_hessian, const MatrixXd &init_pix_jacobian,
		const MatrixXd &init_pix_hessian) override;
	void cmptCurrHessian(MatrixXd &curr_hessian, const MatrixXd &curr_pix_jacobian,
		const MatrixXd &curr_pix_hessian) override;

	void cmptSelfHessian(MatrixXd &self_hessian, 
		const MatrixXd &curr_pix_jacobian) override{
		cmptCurrHessian(self_hessian, curr_pix_jacobian);
	}
	void cmptSelfHessian(MatrixXd &self_hessian, const MatrixXd &curr_pix_jacobian,
		const MatrixXd &curr_pix_hessian) override{
		cmptSelfHessian(self_hessian, curr_pix_jacobian);
	}
	//! analogous to cmptDifferenceOfJacobians except for computing the difference between the current and initial Hessians
	void cmptSumOfHessians(MatrixXd &sum_of_hessians,
		const MatrixXd &init_pix_jacobian,
		const MatrixXd &curr_pix_jacobian) override{
#ifndef DISABLE_SPI
		if(spi_mask){
			getSumOfHessians(sum_of_hessians, spi_mask,
				init_pix_jacobian, curr_pix_jacobian);
		} else{
#endif
			sum_of_hessians.noalias() = -(init_pix_jacobian.transpose() * init_pix_jacobian
				+ curr_pix_jacobian.transpose() * curr_pix_jacobian);
#ifndef DISABLE_SPI
		}
#endif
	}
	void cmptSumOfHessians(MatrixXd &sum_of_hessians,
		const MatrixXd &init_pix_jacobian, const MatrixXd &curr_pix_jacobian,
		const MatrixXd &init_pix_hessian, const MatrixXd &curr_pix_hessian) override;

	/*Support for FLANN library*/
	typedef bool is_kdtree_distance;

	typedef double ElementType;
	typedef double ResultType;
	/**
	* Squared Euclidean distance functor, optimized version
	*/
	/**
	*  Compute the squared Euclidean distance between two vectors.
	*
	*	This is highly optimized, with loop unrolling, as it is one
	*	of the most expensive inner loops.
	*
	*	The computation of squared root at the end is omitted for
	*	efficiency.
	*/
	double operator()(const double* a, const double* b, 
		size_t size, double worst_dist = -1) const override;
	/**
	*	Partial euclidean distance, using just one dimension. This is used by the
	*	kd-tree when computing partial distances while traversing the tree.
	*
	*	Squared root is omitted for efficiency.
	*/
	//template <typename U, typename V>
	//ResultType accum_dist(const U& a, const V& b, int) const
	//{
	//	return (a - b)*(a - b);
	//}
	double accum_dist(const double& a, const double& b, int) const{
		return (a - b)*(a - b);
	}
	void updateDistFeat(double* feat_addr) override{
		for(size_t pix = 0; pix < n_pix; pix++) {
			*feat_addr++ = curr_pix_vals(pix);
		}
	}
	void initializeDistFeat() override{}
	void updateDistFeat() override{}
	const double* getDistFeat() override{ return getCurrPixVals().data(); }
	int getDistFeatSize() override{ return n_pix; }

private:
	// functions to provide support for SPI
	void getJacobian(RowVectorXd &jacobian, const bool *pix_mask,
		const RowVectorXd &curr_grad, const MatrixXd &pix_jacobian);
	void getHessian(MatrixXd &hessian, const bool *pix_mask, const MatrixXd &pix_jacobian);
	void getDifferenceOfJacobians(RowVectorXd &diff_of_jacobians, const bool *pix_mask,
		const MatrixXd &init_pix_jacobian, const MatrixXd &curr_pix_jacobian);
	void getSumOfHessians(MatrixXd &sum_of_hessians, const bool *pix_mask,
		const MatrixXd &init_pix_jacobian, const MatrixXd &curr_pix_jacobian);
};

_MTF_END_NAMESPACE

#endif