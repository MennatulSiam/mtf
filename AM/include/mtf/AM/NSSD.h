#ifndef NSSD_H
#define NSSD_H

#include "SSDBase.h"

#define DEF_NORM_MAX 1.0
#define DEF_NORM_MIN 0.0
#define DEF_DEBUG false

_MTF_BEGIN_NAMESPACE

struct NSSDParams : ImgParams{
	double norm_pix_min; //! minimum pix value after normalization
	double norm_pix_max; //! maximum pix value after normalization
	bool debug_mode; //! decides whether logging data will be printed for debugging purposes; 
	//! only matters if logging is enabled at compile time

	//! value constructor
	NSSDParams(ImgParams *img_params,
		double _norm_pix_max, double _norm_pix_min,
		bool _debug_mode):
		ImgParams(img_params){
			norm_pix_max = _norm_pix_max;
			norm_pix_min = _norm_pix_min;
			debug_mode = _debug_mode;
	}
	//! default/copy constructor
	NSSDParams(NSSDParams *params = nullptr) :
		ImgParams(params),
		norm_pix_min(DEF_NORM_MIN),
		norm_pix_max(DEF_NORM_MAX),
		debug_mode(DEF_DEBUG){
			if(params){
				norm_pix_max = params->norm_pix_max;
				norm_pix_min = params->norm_pix_min;
				debug_mode = params->debug_mode;
			}
	}
};
class NSSD : public SSDBase{
public:

	typedef NSSDParams ParamType; 

	ParamType params;
	NSSD(ParamType *nssd_params);
	NSSD() : SSDBase(){}
};

_MTF_END_NAMESPACE

#endif