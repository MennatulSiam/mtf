#include "mtf/SM/NN.h"
#include <time.h>
#include <ctime> 
#include <flann/io/hdf5.h>
#include "mtf/Utilities/miscUtils.h"
#include <fstream> 

#ifdef _WIN32
#include <time.h>
#define start_db_timer() \
	clock_t db_start_time = clock()
#define start_idx_timer() \
	clock_t idx_start_time = clock()
#define end_db_timer(db_time) \
	clock_t db_end_time = clock();\
	db_time = static_cast<double>(db_end_time - db_start_time)/CLOCKS_PER_SEC
#define end_idx_timer(idx_time) \
	clock_t idx_end_time = clock();\
	idx_time = static_cast<double>(idx_end_time - idx_start_time)/CLOCKS_PER_SEC
#else
#include <ctime> 
#define start_db_timer() \
	timespec db_start_time;\
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &db_start_time)
#define start_idx_timer() \
	timespec idx_start_time;\
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &idx_start_time)
#define end_db_timer(db_time) \
	timespec db_end_time;\
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &db_end_time);\
	db_time = ((double)(db_end_time.tv_sec - db_start_time.tv_sec) +\
		1e-9*(double)(db_end_time.tv_nsec - db_start_time.tv_nsec))
#define end_idx_timer(idx_time) \
	timespec idx_end_time;\
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &idx_end_time);\
	idx_time = ((double)(idx_end_time.tv_sec - idx_start_time.tv_sec) +\
		1e-9*(double)(idx_end_time.tv_nsec - idx_start_time.tv_nsec))
#endif

_MTF_BEGIN_NAMESPACE

template <class AM, class SSM>
NN<AM, SSM >::NN(
	ParamType *nn_params, IdxParamType *nn_idx_params,
	AMParams *am_params, SSMParams *ssm_params) :
	SearchMethod<AM, SSM>(am_params, ssm_params),
	params(nn_params), idx_params(nn_idx_params){
	printf("\n");
	printf("initializing Nearest Neighbor tracker with:\n");
	printf("max_iters: %d\n", params.max_iters);
	printf("n_samples: %d\n", params.n_samples);
	printf("epsilon: %f\n", params.epsilon);
	if(params.pix_sigma <= 0){
		if(params.ssm_sigma.size() < ssm->getStateSize()){
			throw std::invalid_argument(
				cv::format("NN :: SSM sigma has too few values %lu\n",
				params.ssm_sigma.size()));
		}
		printf("ssm_sigma: \n");
		for(int state_id = 0; state_id < ssm->getStateSize(); state_id++){
			printf("%f ", params.ssm_sigma[state_id]);
		}
		printf("\n");
	} else{
		printf("pix_sigma: %f\n", params.pix_sigma);
	}

	printf("corner_sigma_d: %f\n", params.corner_sigma_d);
	printf("corner_sigma_t: %f\n", params.corner_sigma_t);

	printf("index_type: %d (%s)\n", params.index_type,
		NNParams::toString(params.index_type));
	printf("n_checks: %d\n", params.n_checks);
	printf("additive_update: %d\n", params.additive_update);
	printf("direct_samples: %d\n", params.direct_samples);
	printf("save_index: %d\n", params.save_index);
	printf("ssm_sigma_prec: %f\n", params.ssm_sigma_prec);
	printf("debug_mode: %d\n", params.debug_mode);

	printf("appearance model: %s\n", am->name.c_str());
	printf("state space model: %s\n", ssm->name.c_str());
	printf("\n");

	name = "nn";
	log_fname = "log/mtf_nn_log.txt";
	time_fname = "log/mtf_nn_times.txt";
	frame_id = 0;

	ssm_state_size = ssm->getStateSize();
	am_dist_size = am->getDistFeatSize();

	state_sigma.resize(ssm_state_size);
	state_sigma = VectorXdM(params.ssm_sigma.data(), ssm_state_size);

	utils::printMatrix(state_sigma, "state_sigma");

	printf("ssm_state_size: %d\n", ssm_state_size);
	printf("am_dist_size: %d\n", am_dist_size);

	//flann_query = new flannMatT(const_cast<double*>(eig_query.data()), 
	//	eig_query.rows(), eig_query.cols());

	//flann_result = new flannResultT(const_cast<int*>(eig_result.data()),
	//	eig_result.rows(), eig_result.cols());
	//flann_dists = new flannMatT(const_cast<double *>(eig_dists.data()),
	//	eig_dists.rows(), eig_dists.cols());

	eig_dataset.resize(params.n_samples, am_dist_size);
	//eig_query.resize(am_dist_size);
	eig_result.resize(1);
	eig_dists.resize(1);

	flann_dataset = new flannMatT(const_cast<double *>(eig_dataset.data()),
		eig_dataset.rows(), eig_dataset.cols());

	ssm_update.resize(ssm_state_size);
	ssm_perturbations.resize(ssm_state_size, params.n_samples);
	inv_state_update.resize(ssm_state_size);

	string fname_template = cv::format("%s_%s_%s_%d_%d", am->name.c_str(), ssm->name.c_str(),
		NNParams::toString(params.index_type), params.n_samples, am_dist_size);
	saved_db_path = cv::format("%s/%s.db", params.saved_index_dir.c_str(), fname_template.c_str());
	saved_idx_path = cv::format("%s/%s.idx", params.saved_index_dir.c_str(), fname_template.c_str());

	//pix_grad_identity.resize(am->getPixCount(), Eigen::NoChange);
	//pix_grad_identity.fill(1);
	//ssm_grad_norm.resize(am->getPixCount(), ssm_state_size);
	//ssm_grad_norm_mean.resize(ssm_state_size);
}

template <class AM, class SSM>
void NN<AM, SSM >::initialize(const cv::Mat &corners){
	start_timer();

	am->clearInitStatus();
	ssm->clearInitStatus();

	ssm->initialize(corners);

	if(params.pix_sigma <= 0){
		ssm->initializeSampler(state_sigma);
	} else{
		ssm->initializeSampler(params.pix_sigma);
	}

	am->initializePixVals(ssm->getPts());
	am->initializeDistFeat();

	//utils::printMatrix(ssm->getCorners(), "init_corners original");
	//utils::printMatrix(ssm->getCorners(), "init_corners after");
	//utils::printScalarToFile("initializing NN...", " ", log_fname, "%s", "w");

	if(params.load_index){
		ifstream in_file(saved_db_path, ios::in | ios::binary);
		if(in_file.good()){
			printf("Loading feature dataset from: %s\n", saved_db_path.c_str());
			start_db_timer();
			in_file.read((char*)(eig_dataset.data()), sizeof(double)*eig_dataset.size());
			in_file.read((char*)(ssm_perturbations.data()), sizeof(double)*ssm_perturbations.size());
			in_file.close();
			double db_time;
			end_db_timer(db_time);
			printf("Time taken: %f secs\n", db_time);
			params.save_index = false;
		} else{
			printf("Failed to load feature dataset from: %s\n", saved_db_path.c_str());
			params.load_index = false;
		}
	}
	if(!params.load_index){
		VectorXd state_update(ssm_state_size);
		printf("building feature dataset...\n");
		start_db_timer();
		for(int sample_id = 0; sample_id < params.n_samples; ++sample_id){
			//utils::printMatrix(ssm->getCorners(), "Corners before");
			ssm->generatePerturbation(state_update);
			//utils::printMatrix(state_update, "state_update");

			if(params.additive_update){
				inv_state_update = -state_update;
				ssm->additiveUpdate(inv_state_update);
			} else{
				ssm->invertState(inv_state_update, state_update);
				ssm->compositionalUpdate(inv_state_update);
			}
			//utils::printMatrix(inv_state_update, "inv_state_update");

			am->updatePixVals(ssm->getPts());
			am->updateDistFeat(eig_dataset.row(sample_id).data());

			ssm_perturbations.col(sample_id) = state_update;

			// reset SSM to previous state
			if(params.additive_update){
				ssm->additiveUpdate(state_update);
			} else{
				ssm->compositionalUpdate(state_update);
			}
			//utils::printMatrix(ssm->getCorners(), "Corners after");
		}
		double db_time;
		end_db_timer(db_time);
		printf("Time taken: %f secs\n", db_time);
		printf("building flann index...\n");
	}
	start_idx_timer();
	flann_index = new flannIdxT(*flann_dataset, getIndexParams(), *am);
	flann_index->buildIndex();
	double idx_time;
	end_idx_timer(idx_time);
	printf("Time taken: %f secs\n", idx_time);

	if(params.save_index){		
		ofstream out_file(saved_db_path, ios::out | ios::binary);
		if(out_file.good()){
			printf("Saving dataset to: %s\n", saved_db_path.c_str());
			out_file.write((char*)(eig_dataset.data()), sizeof(double)*eig_dataset.size());
			out_file.write((char*)(ssm_perturbations.data()), sizeof(double)*ssm_perturbations.size());
			out_file.close();
			printf("Saving index to: %s\n", saved_idx_path.c_str());
			flann_index->save(saved_idx_path);
		} else{
			printf("Failed to saved dataset to: %s\n", saved_db_path.c_str());
		}
	}
	if(params.debug_mode){
		flann::save_to_file<double>(*flann_dataset, "log/flann_log.hdf5", "flann_dataset");
	}
	ssm->getCorners(cv_corners_mat);

	end_timer();
	write_interval(time_fname, "w");
}

template <class AM, class SSM>
void NN<AM, SSM >::update(){
	++frame_id;
	write_frame_id(frame_id);

	am->setFirstIter();
	for(int i = 0; i < params.max_iters; i++){
		init_timer();

		am->updatePixVals(ssm->getPts());
		record_event("am->updatePixVals");

		am->updateDistFeat();
		record_event("am->updateDistFeat");

		flannMatT flann_query(const_cast<double*>(am->getDistFeat()), 1, am_dist_size);
		record_event("am->updateDistFeat");

		flannResultT flann_result(&best_idx, 1, 1);
		flannMatT flann_dists(&best_dist, 1, 1);
		record_event("flann_dists/result");

		flann_index->knnSearch(flann_query, flann_result, flann_dists, 1, flann::SearchParams(params.n_checks));
		ssm_update = ssm_perturbations.col(best_idx);
		record_event("flann_index->knnSearch");

		//if(params.debug_mode){
		//	printf("The nearest neighbor is at index %d with distance %f\n", best_idx, best_dist);
		//	//flann::save_to_file(flann_result, "log/flann_log.hdf5", "flann_result");
		//	//flann::save_to_file(flann_dists, "log/flann_log.hdf5", "flann_dists");
		//	//utils::printMatrix(ssm_update, "ssm_update");
		//}

		prev_corners = ssm->getCorners();

		if(params.additive_update){
			ssm->additiveUpdate(ssm_update);
			record_event("ssm->additiveUpdate");
		} else{
			ssm->compositionalUpdate(ssm_update);
			record_event("ssm->compositionalUpdate");
		}

		double update_norm = (prev_corners - ssm->getCorners()).squaredNorm();
		record_event("update_norm");

		write_data(time_fname);

		if(update_norm < params.epsilon){
			if(params.debug_mode){
				printf("n_iters: %d\n", i + 1);
			}
			break;
		}

		am->clearFirstIter();
	}
	ssm->getCorners(cv_corners_mat);
}

template <class AM, class SSM>
const flann::IndexParams NN<AM, SSM >::getIndexParams(){
	if(params.load_index){
		printf("Loading SavedIndex from: %s\n", saved_idx_path.c_str());
		return flann::SavedIndexParams(saved_idx_path);
	}
	switch(params.index_type){
	case IdxType::Linear:
		printf("Using Linear index\n");
		return flann::LinearIndexParams();
	case IdxType::KDTree:
		printf("Using KD Tree index with:\n");
		printf("n_trees: %d\n", idx_params.kdt_trees);
		return flann::KDTreeIndexParams(idx_params.kdt_trees);
	case IdxType::KMeans:
		if(!(idx_params.km_centers_init == flann::FLANN_CENTERS_RANDOM ||
			idx_params.km_centers_init == flann::FLANN_CENTERS_GONZALES ||
			idx_params.km_centers_init == flann::FLANN_CENTERS_KMEANSPP)){
			printf("Invalid method provided for selecting initial centers: %d. Using random centers...\n",
				idx_params.km_centers_init);
			idx_params.km_centers_init = flann::FLANN_CENTERS_RANDOM;
		}
		printf("Using KMeans index with:\n");
		printf("branching: %d\n", idx_params.km_branching);
		printf("iterations: %d\n", idx_params.km_iterations);
		printf("centers_init: %d\n", idx_params.km_centers_init);
		printf("cb_index: %f\n", idx_params.km_cb_index);
		return flann::KMeansIndexParams(idx_params.km_branching, idx_params.km_iterations,
			idx_params.km_centers_init, idx_params.km_cb_index);
	case IdxType::Composite:
		if(!(idx_params.km_centers_init == flann::FLANN_CENTERS_RANDOM ||
			idx_params.km_centers_init == flann::FLANN_CENTERS_GONZALES ||
			idx_params.km_centers_init == flann::FLANN_CENTERS_KMEANSPP)){
			printf("Invalid method provided for selecting initial centers: %d. Using random centers...\n", idx_params.km_centers_init);
			idx_params.km_centers_init = flann::FLANN_CENTERS_RANDOM;
		}
		printf("Using Composite index with:\n");
		printf("n_trees: %d\n", idx_params.kdt_trees);
		printf("branching: %d\n", idx_params.km_branching);
		printf("iterations: %d\n", idx_params.km_iterations);
		printf("centers_init: %d\n", idx_params.km_centers_init);
		printf("cb_index: %f\n", idx_params.km_cb_index);
		return flann::CompositeIndexParams(idx_params.kdt_trees, idx_params.km_branching, idx_params.km_iterations,
			idx_params.km_centers_init, idx_params.km_cb_index);
	case IdxType::HierarchicalClustering:
		if(!(idx_params.hc_centers_init == flann::FLANN_CENTERS_RANDOM ||
			idx_params.hc_centers_init == flann::FLANN_CENTERS_GONZALES ||
			idx_params.hc_centers_init == flann::FLANN_CENTERS_KMEANSPP)){
			printf("Invalid method provided for selecting initial centers: %d. Using random centers...\n",
				idx_params.km_centers_init);
			idx_params.hc_centers_init = flann::FLANN_CENTERS_RANDOM;
		}
		printf("Using Hierarchical Clustering index with:\n");
		printf("branching: %d\n", idx_params.hc_branching);
		printf("centers_init: %d\n", idx_params.hc_centers_init);
		printf("trees: %d\n", idx_params.hc_trees);
		printf("leaf_max_size: %d\n", idx_params.hc_leaf_max_size);
		return flann::HierarchicalClusteringIndexParams(idx_params.hc_branching,
			idx_params.hc_centers_init, idx_params.hc_trees, idx_params.hc_leaf_max_size);
	case IdxType::KDTreeSingle:
		printf("Using KDTreeSingle index with:\n");
		printf("leaf_max_size: %d\n", idx_params.kdtc_leaf_max_size);
		return flann::KDTreeSingleIndexParams(idx_params.kdts_leaf_max_size);
		//case KDTreeCuda3d:
		//	printf("Using KDTreeCuda3d index with:\n");
		//	printf("leaf_max_size: %d\n", kdtc_leaf_max_size);
		//	return flann::KDTreeCuda3dIndexParams(kdtc_leaf_max_size);
	case IdxType::Autotuned:
		printf("Using Autotuned index with:\n");
		printf("target_precision: %f\n", idx_params.auto_target_precision);
		printf("build_weight: %f\n", idx_params.auto_build_weight);
		printf("memory_weight: %f\n", idx_params.auto_memory_weight);
		printf("sample_fraction: %f\n", idx_params.auto_sample_fraction);
		return flann::AutotunedIndexParams(idx_params.auto_target_precision, idx_params.auto_build_weight,
			idx_params.auto_memory_weight, idx_params.auto_sample_fraction);
	default: printf("Invalid index type specified: %d. Using KD Tree index by default...\n",
		params.index_type);
		return flann::KDTreeIndexParams(idx_params.kdt_trees);
	}
}


//template <class AM, class SSM>
//double NN<AM, SSM >::getCornerChangeNorm(const VectorXd &state_update){
//
//	utils::printMatrix(ssm->getCorners(), "corners before");
//	utils::printMatrix(state_update, "state_update");
//	ssm->compositionalUpdate(state_update);
//
//	utils::printMatrix(ssm->getCorners(), "corners middle");
//	//utils::printMatrix(ssm->getCorners(), "init corners");
//
//	Matrix24d corner_change = ssm->getCorners() - ssm->getCorners();
//	utils::printMatrix(corner_change, "corner_change");
//	double corner_change_norm = corner_change.lpNorm<1>() / 8;
//	ssm->invertState(inv_state_update, state_update);
//	ssm->compositionalUpdate(inv_state_update);
//	//utils::printMatrix(ssm->getCorners(), "corners after");
//	return corner_change_norm;
//}
//
//template <class AM, class SSM>
//double NN<AM, SSM >::findSSMSigma(int param_id){
//	double param_std = params.corner_sigma_d;
//	VectorXd state_update(ssm->getStateSize());
//	Matrix3d inv_warp;
//	state_update.fill(0);
//	Matrix24d corner_change;
//	while(true){
//		state_update(param_id) = param_std;
//
//		double corner_change_norm = getCornerChangeNorm(state_update);
//		double perturbation_ratio = corner_change_norm / params.corner_sigma_d;
//
//		if(perturbation_ratio > params.ssm_sigma_prec){
//			param_std /= params.ssm_sigma_prec;
//		} else if(perturbation_ratio < 1 / params.ssm_sigma_prec){
//			param_std *= params.ssm_sigma_prec;
//		} else{
//			return param_std;
//		}
//	}
//}
//
//template <class AM, class SSM>
//double NN<AM, SSM >::findSSMSigmaGrad(int param_id){
//	double param_std = params.corner_sigma_d;
//	VectorXd state_update(ssm->getStateSize());
//	state_update.fill(0);
//	state_update(param_id) = param_std + NN_CORNER_GRAD_EPS;
//	double corner_change_inc = getCornerChangeNorm(state_update);
//	state_update(param_id) = param_std - NN_CORNER_GRAD_EPS;
//	double corner_change_dec = getCornerChangeNorm(state_update);
//	double corner_change_grad = (corner_change_inc - corner_change_dec) / (2 * NN_CORNER_GRAD_EPS);
//	double ssm_sigma = params.corner_sigma_d / corner_change_grad;
//
//	utils::printScalar(param_id, "\nparam_id", "%d");
//	utils::printScalar(corner_change_inc, "corner_change_inc");
//	utils::printScalar(corner_change_dec, "corner_change_dec");
//	utils::printScalar(corner_change_grad, "corner_change_grad");
//	utils::printScalar(ssm_sigma, "ssm_sigma");
//
//	return ssm_sigma;
//}


const char* NNParams::toString(IdxType index_type){
	switch(index_type){
	case IdxType::KDTree:
		return "KDTree";
	case IdxType::HierarchicalClustering:
		return "HierarchicalClustering";
	case IdxType::KMeans:
		return "KMeans";
	case IdxType::Composite:
		return "Composite";
	case IdxType::Linear:
		return "Linear";
	case IdxType::KDTreeSingle:
		return "KDTreeSingle";
	case IdxType::KDTreeCuda3d:
		return "KDTreeCuda3d";
	case IdxType::Autotuned:
		return "Autotuned";
	default:
		throw std::invalid_argument("Invalid index type provided");
	}
}

NNParams::NNParams(
	int _max_iters, int _n_samples, double _epsilon,
	const vector<double> &_ssm_sigma,
	double _corner_sigma_d, double _corner_sigma_t,
	double _pix_sigma, IdxType _index_type, int _n_checks,
	bool _additive_update, bool _direct_samples,
	bool _load_index, bool _save_index, string _saved_index_dir,
	double _ssm_sigma_prec, bool _debug_mode) :
	max_iters(_max_iters),
	n_samples(_n_samples),
	epsilon(_epsilon),
	ssm_sigma(_ssm_sigma),
	corner_sigma_d(_corner_sigma_d),
	corner_sigma_t(_corner_sigma_t),
	pix_sigma(_pix_sigma),
	index_type(_index_type),
	n_checks(_n_checks),
	additive_update(_additive_update),
	direct_samples(_direct_samples),
	load_index(_load_index),
	save_index(_save_index),
	saved_index_dir(_saved_index_dir),
	ssm_sigma_prec(_ssm_sigma_prec),
	debug_mode(_debug_mode){}

NNParams::NNParams(NNParams *params) :
max_iters(NN_MAX_ITERS),
n_samples(NN_N_SAMPLES),
epsilon(NN_EPSILON),
corner_sigma_d(NN_CORNER_SIGMA_D),
corner_sigma_t(NN_CORNER_SIGMA_T),
pix_sigma(NN_PIX_SIGMA),
index_type(static_cast<IdxType>(NN_INDEX_TYPE)),
n_checks(NN_N_CHECKS),
additive_update(NN_ADDITIVE_UPDATE),
direct_samples(NN_DIRECT_SAMPLES),
load_index(NN_LOAD_INDEX),
save_index(NN_SAVE_INDEX),
saved_index_dir(NN_INDEX_FILE_TEMPLATE),
ssm_sigma_prec(NN_SSM_SIGMA_PREC),
debug_mode(NN_DEBUG_MODE){
	if(params){
		max_iters = params->max_iters;
		n_samples = params->n_samples;
		epsilon = params->epsilon;

		ssm_sigma = params->ssm_sigma;
		corner_sigma_d = params->corner_sigma_d;
		corner_sigma_t = params->corner_sigma_t;
		pix_sigma = params->pix_sigma;

		index_type = params->index_type;
		n_checks = params->n_checks;
		additive_update = params->additive_update;
		direct_samples = params->direct_samples;
		load_index = params->load_index;
		save_index = params->save_index;
		saved_index_dir = params->saved_index_dir;
		ssm_sigma_prec = params->ssm_sigma_prec;
		debug_mode = params->debug_mode;
	}
}

NNIndexParams::NNIndexParams(
	int _kdt_trees,
	int _km_branching,
	int _km_iterations,
	flann::flann_centers_init_t _km_centers_init,
	float _km_cb_index,
	int _kdts_leaf_max_size,
	int _kdtc_leaf_max_size,
	int _hc_branching,
	flann::flann_centers_init_t _hc_centers_init,
	int _hc_trees,
	int _hc_leaf_max_size,
	float _auto_target_precision,
	float _auto_build_weight,
	float _auto_memory_weight,
	float _auto_sample_fraction) :
	kdt_trees(_kdt_trees),
	km_branching(_km_branching),
	km_iterations(_km_iterations),
	km_centers_init(_km_centers_init),
	km_cb_index(_km_cb_index),
	kdts_leaf_max_size(_kdts_leaf_max_size),
	kdtc_leaf_max_size(_kdtc_leaf_max_size),
	hc_branching(_hc_branching),
	hc_centers_init(_hc_centers_init),
	hc_trees(_hc_trees),
	hc_leaf_max_size(_hc_leaf_max_size),
	auto_target_precision(_auto_target_precision),
	auto_build_weight(_auto_build_weight),
	auto_memory_weight(_auto_memory_weight),
	auto_sample_fraction(_auto_sample_fraction){}

NNIndexParams::NNIndexParams(NNIndexParams *params) :
kdt_trees(NN_KDT_TREES),
km_branching(NN_KM_BRANCHING),
km_iterations(NN_KM_ITERATIONS),
km_centers_init(NN_KM_CENTERS_INIT),
km_cb_index(NN_KM_CB_INDEX),
kdts_leaf_max_size(NN_KDTS_LEAF_MAX_SIZE),
kdtc_leaf_max_size(NN_KDTC_LEAF_MAX_SIZE),
hc_branching(NN_HC_BRANCHING),
hc_centers_init(NN_HC_CENTERS_INIT),
hc_trees(NN_HC_TREES),
hc_leaf_max_size(NN_HC_LEAF_MAX_SIZE),
auto_target_precision(NN_AUTO_TARGET_PRECISION),
auto_build_weight(NN_AUTO_BUILD_WEIGHT),
auto_memory_weight(NN_AUTO_MEMORY_WEIGHT),
auto_sample_fraction(NN_AUTO_SAMPLE_FRACTION){
	if(params){
		kdt_trees = params->kdt_trees;
		km_branching = params->km_branching;
		km_iterations = params->km_iterations;
		km_centers_init = params->km_centers_init;
		km_cb_index = params->km_cb_index;
		kdts_leaf_max_size = params->kdts_leaf_max_size;
		kdtc_leaf_max_size = params->kdtc_leaf_max_size;
		hc_branching = params->hc_branching;
		hc_centers_init = params->hc_centers_init;
		hc_trees = params->hc_trees;
		hc_leaf_max_size = params->hc_leaf_max_size;
		auto_target_precision = params->auto_target_precision;
		auto_build_weight = params->auto_build_weight;
		auto_memory_weight = params->auto_memory_weight;
		auto_sample_fraction = params->auto_sample_fraction;
	}
}

_MTF_END_NAMESPACE

#include "mtf/Macros/register.h"
_REGISTER_TRACKERS(NN);
