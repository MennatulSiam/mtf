
#include "memwatch.h"
#include "build_graph.h"
#include "search_graph_knn.h"
#include "utility.h"

int X_cols;
int X_rows;
int num_query; 

/*...................................................*/
int main()
{

  srand(1000);
  char resPath[] = "Data/Results/"; //"TestRes/graph_c/1nn/";
  //int DB_size[] = {5000}; //{17056, 50000, 118607, 204727};
 
  X_rows = 5000; // size of the dataset
  X_cols = 128; // dimension of data
  num_query = 50; 
  struct node *Nodes;
 // int *X;, *Xq; 

  char data_file[] = "Data/vocab_5000.dat";
  printf("Reading in data...");
  double *X = Read_in_data(data_file, X_rows);

 // Build graph
  printf("Building the graph...\n"); fflush(NULL);
  int k = 250;
  Nodes = build_graph(X, k);

  char graph_file[] = "Data/graph_250nn_5000nodes.dat";
  printf("Writing into file...\n");  fflush(NULL);
  fwrite_graph(Nodes, graph_file, X_rows);
  
  delete_graph(Nodes);
  free(X);

////////// linear search
/*  struct timeval time1, time2;
  double elapsed;
  int *query = malloc(X_cols*sizeof(int));
//  query[0] = Xq[0];
  struct indx_dist *tnn_dists = malloc(X_rows * sizeof(struct indx_dist)); 
  check_pointer(tnn_dists, "Couldn't malloc tnn_dists");
  for (int t=0; t<num_query; t++)
  { 
    printf("q:%d ", t); fflush(NULL);
    for (int j=0; j<X_cols; j++)
        1query[j] = Xq[t*X_cols+j];

    gettimeofday(&time1, NULL); //t1 = clock();
    
    knn_search2(query, tnn_dists, X, X_rows, X_cols, K);
    
    gettimeofday(&time2, NULL); //elapsed = clock()-t1;
    elapsed = (time2.tv_sec*pow(10,6) + time2.tv_usec) -
           (time1.tv_sec*pow(10,6) + time1.tv_usec);
    printf("dist: %lf, time: %lf\n", tnn_dists[0], elapsed/1000000.0); 
  }
  free(query);
  free(tnn_dists);*/

// knn search in graph
  int K=1; // the number of nearest neighbors to return

//  char data_file[] = "Data/vocab_5000.dat"; 
  printf("Reading in data...");
  X = Read_in_data(data_file, X_rows);

  char filename[] = "Data/res.txt";
  printf("Output goes to: %s\n", filename);
    
  //----------------------
  printf("Reading test data file...\n");
  char testfile[] = "Data/queries.dat";
  printf("testfile: %s\n", testfile);
  double *Xq = Read_in_data(testfile, num_query);
  //-----------------------
  printf("Reading in the graph...\n"); fflush(NULL);
  k = 250; // in k-NN graph
  //char graph_file[] = "Data/graph_250nn_5000nodes.dat"; 
  Nodes = fread_graph(graph_file);

  
  char resMat[] = "graph_250nn_5000nodes_1nn"; 
  printf("%s\n", resMat);
 
  int I=1;
  struct stats* Stat = allocate_stats(I, num_query);
  search_graph(Nodes, Xq, X, k, K, &Stat[I-1]);

  delete_graph(Nodes);
  write2file_stats(Stat, I, k, resPath, filename, resMat);
  delete_stats(Stat, I);
 
  // freeing memory
  free(X);
  free(Xq);

  return 0;
}





