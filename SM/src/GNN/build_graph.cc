#include "memwatch.h"
#include "build_graph.h"

namespace gnn{
	//--------------------------------
	void add_node(struct node *node_i, int nn)
	{
		int size = node_i->size++;
		if(size >= node_i->capacity)
		{
			node_i->nns_inds = static_cast<int*>(realloc(node_i->nns_inds, /*size*2*/ (size + 10)*sizeof(int)));
			node_i->capacity = /*size*2*/ size + 10;
		}
		node_i->nns_inds[size] = nn;
	}
}



