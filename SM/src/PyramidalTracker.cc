#include "mtf/SM/PyramidalTracker.h"
#include "mtf/Utilities/miscUtils.h"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

_MTF_BEGIN_NAMESPACE

PyramidalParams::PyramidalParams(int _no_of_levels, double _scale_factor,
bool _show_levels) {
	no_of_levels = _no_of_levels;
	scale_factor = _scale_factor;
	show_levels = _show_levels;
}
PyramidalParams::PyramidalParams(PyramidalParams *params) :
no_of_levels(PYRL_NO_OF_LEVELS),
scale_factor(PYRL_SCALE_FACTOR){
	if(params) {
		no_of_levels = params->no_of_levels;
		scale_factor = params->scale_factor;
		show_levels = params->show_levels;
	}
}
PyramidalTracker::PyramidalTracker(const vector<TrackerBase*> _trackers,
	ParamType *parl_params) : CompositeBase(_trackers),
	params(parl_params){
	if(n_trackers != params.no_of_levels){
		throw std::domain_error(
			cv::format("PyramidalTracker :: no. of trackers: %d does not match the no. of levels in the pyramid: %d",
			n_trackers, params.no_of_levels));
	}
	if(input_type == HETEROGENEOUS_INPUT){
		throw std::domain_error("PyramidalTracker :: Heterogeneous input is currently not supported");
	}
	printf("\n");
	printf("Initializing Pyramidal tracker with:\n");
	printf("no_of_levels: %d\n", params.no_of_levels);
	printf("scale_factor: %f\n", params.scale_factor);
	printf("trackers: ");
	name = "pyr: " + trackers[0]->name;
	for(int tracker_id = 1; tracker_id < n_trackers; tracker_id++) {
		if(trackers[tracker_id]->name != trackers[0]->name){
			name = name + trackers[tracker_id]->name + " ";
			printf("%d: %s ", tracker_id + 1, trackers[tracker_id]->name.c_str());
		}
	}
	printf("\n");
	img_pyramid.resize(params.no_of_levels);
	img_sizes.resize(params.no_of_levels);
	scale_factor = pow(params.scale_factor, params.no_of_levels - 1);
}
void PyramidalTracker::setImage(const cv::Mat &img){
	if(img_pyramid[0].empty()){
		img_sizes[0] = cv::Size(img.cols, img.rows);
		printf("Level %d: size: %dx%d\n",
			0, img_sizes[0].width, img_sizes[0].height);
	}

	img_pyramid[0] = img;
	trackers[n_trackers - 1]->setImage(img_pyramid[0]);
	for(int pyr_level = 1; pyr_level < params.no_of_levels; ++pyr_level){
		if(img_pyramid[pyr_level].empty()){
			int n_rows = img_pyramid[pyr_level - 1].rows*params.scale_factor;
			int n_cols = img_pyramid[pyr_level - 1].cols*params.scale_factor;
			img_sizes[pyr_level] = cv::Size(n_cols, n_rows);
			img_pyramid[pyr_level].create(img_sizes[pyr_level], img.type());
			printf("Level %d: size: %dx%d\n",
				pyr_level, img_sizes[pyr_level].width, img_sizes[pyr_level].height);
		}
		trackers[n_trackers - pyr_level - 1]->setImage(img_pyramid[pyr_level]);
	}
}
void PyramidalTracker::updateImagePyramid(){
	for(int pyr_level = 1; pyr_level < params.no_of_levels; ++pyr_level){
		if(params.scale_factor == 0.5){
			cv::pyrDown(img_pyramid[pyr_level - 1], img_pyramid[pyr_level], img_sizes[pyr_level]);
		} else{
			cv::resize(img_pyramid[pyr_level - 1], img_pyramid[pyr_level], img_sizes[pyr_level]);
			cv::GaussianBlur(img_pyramid[pyr_level], img_pyramid[pyr_level], cv::Size(5, 5), 3);
		}		
	}
}
void PyramidalTracker::initialize(const cv::Mat &corners)  {
	updateImagePyramid();
	trackers[n_trackers-1]->initialize(corners);
	cv::Mat scaled_corners = corners.clone();
	for(int pyr_level = 1; pyr_level < params.no_of_levels; ++pyr_level){
		scaled_corners *= params.scale_factor;
		trackers[n_trackers - pyr_level - 1]->initialize(scaled_corners);
	}
	if(params.show_levels){
		for(int pyr_level = 1; pyr_level < params.no_of_levels; ++pyr_level){
			cv::namedWindow(cv::format("Level %d", pyr_level));
		}
		showImagePyramid();
	}
}
void PyramidalTracker::update()  {
	updateImagePyramid();
	trackers[0]->update();
	for(int tracker_id = 1; tracker_id < n_trackers; ++tracker_id) {	
		cv::Mat scaled_corners = trackers[tracker_id - 1]->getRegion() / params.scale_factor;
		trackers[tracker_id]->setRegion(scaled_corners);
		trackers[tracker_id]->update();
	}
	cv::Mat scaled_corners = trackers[n_trackers - 1]->getRegion() * scale_factor;
	trackers[0]->setRegion(scaled_corners);
	if(params.show_levels){	showImagePyramid(); }
}
void PyramidalTracker::setRegion(const cv::Mat& corners)   {
	trackers[n_trackers - 1]->initialize(corners);
	cv::Mat scaled_corners = corners.clone();
	for(int pyr_level = 1; pyr_level < params.no_of_levels; ++pyr_level){
		scaled_corners *= params.scale_factor;
		trackers[n_trackers - pyr_level - 1]->setRegion(scaled_corners);
	}
	if(params.show_levels){
		updateImagePyramid();
		showImagePyramid();
	}
}

void PyramidalTracker::showImagePyramid(){
	for(int pyr_level = 1; pyr_level < params.no_of_levels; ++pyr_level){
		cv::Mat curr_level_img(img_sizes[pyr_level], CV_8UC3);
		img_pyramid[pyr_level].convertTo(curr_level_img, curr_level_img.type());
		cv::cvtColor(curr_level_img, curr_level_img, CV_GRAY2BGR);
		cv::Scalar line_color(255, 0, 0);
		cv_corners_mat = trackers[n_trackers - pyr_level - 1]->getRegion();
		cv::Point2d ul(cv_corners_mat.at<double>(0, 0), cv_corners_mat.at<double>(1, 0));
		cv::Point2d ur(cv_corners_mat.at<double>(0, 1), cv_corners_mat.at<double>(1, 1));
		cv::Point2d lr(cv_corners_mat.at<double>(0, 2), cv_corners_mat.at<double>(1, 2));
		cv::Point2d ll(cv_corners_mat.at<double>(0, 3), cv_corners_mat.at<double>(1, 3));
		line(curr_level_img, ul, ur, line_color, 2);
		line(curr_level_img, ur, lr, line_color, 2);
		line(curr_level_img, lr, ll, line_color, 2);
		line(curr_level_img, ll, ul, line_color, 2);
		cv::imshow(cv::format("Level %d", pyr_level), curr_level_img);
	}	
}

_MTF_END_NAMESPACE
