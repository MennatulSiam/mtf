#include "mtf/SM/PF.h"
#include <ctime> 
#include "mtf/Utilities/miscUtils.h" 
#include <boost/random/random_device.hpp>
#include <boost/random/seed_seq.hpp>

_MTF_BEGIN_NAMESPACE

template <class AM, class SSM>
PF<AM, SSM >::PF(ParamType *nn_params,
	AMParams *am_params, SSMParams *ssm_params) :
	SearchMethod<AM, SSM>(am_params, ssm_params),
	params(nn_params){

	printf("\n");
	printf("initializing Particle Filter search method with:\n");
	printf("max_iters: %d\n", params.max_iters);
	printf("n_particles: %d\n", params.n_particles);
	printf("epsilon: %f\n", params.epsilon);
	printf("dyn_model: %s\n", PFParams::toString(params.dyn_model));
	printf("upd_type: %s\n", PFParams::toString(params.upd_type));
	printf("likelihood_func: %s\n", PFParams::toString(params.likelihood_func));
	printf("resampling_type: %s\n", PFParams::toString(params.resampling_type));
	printf("reset_to_mean: %d\n", params.reset_to_mean);
	printf("mean_of_corners: %d\n", params.mean_of_corners);

	if(params.pix_sigma <= 0){
		if(params.ssm_sigma.size() < ssm->getStateSize()){
			stringstream err_msg;
			err_msg << "PF :: SSM sigma has invalid size " << params.ssm_sigma.size() << "\n";
			throw std::invalid_argument(err_msg.str());
		}
		printf("ssm_sigma: \n");
		for(int state_id = 0; state_id < ssm->getStateSize(); state_id++){
			printf("%f\t", params.ssm_sigma[state_id]);
		}
		printf("\n");
		state_sigma.resize(ssm->getStateSize());
		state_sigma = VectorXdM(params.ssm_sigma.data(), ssm->getStateSize());
		utils::printMatrix(state_sigma, "state_sigma");
	} else{
		printf("pix_sigma: %f\n", params.pix_sigma);
	}
	printf("measurement_sigma: %f\n", params.measurement_sigma);
	printf("debug_mode: %d\n", params.debug_mode);
	printf("appearance model: %s\n", am->name.c_str());
	printf("state space model: %s\n", ssm->name.c_str());
	printf("\n");

	name = "pf";
	log_fname = "log/mtf_pf_log.txt";
	time_fname = "log/mtf_pf_times.txt";
	frame_id = 0;

	const double pi = 3.14159265358979323846;
	measurement_factor = 1.0 / sqrt(2 * pi * params.measurement_sigma);

	for(int set_id = 0; set_id < 2; ++set_id){
		particle_states[set_id].resize(params.n_particles);
		particle_ar[set_id].resize(params.n_particles);
		for(int particle_id = 0; particle_id < params.n_particles; ++particle_id){
			particle_states[set_id][particle_id].resize(ssm->getStateSize());
			particle_ar[set_id][particle_id].resize(ssm->getStateSize());
		}
	}
	curr_set_id = 0;
	particle_wts.resize(params.n_particles);
	particle_cum_wts.setZero(params.n_particles);

	perturbed_state.resize(ssm->getStateSize());
	perturbed_ar.resize(ssm->getStateSize());
	mean_state.resize(ssm->getStateSize());

	if(params.mean_of_corners){
		particle_corners.resize(params.n_particles);
	}
	// initialize random number generators for resampling and measurement function
	boost::random_device r;
	boost::random::seed_seq measurement_seed{ r(), r(), r(), r(), r(), r(), r(), r() };

	measurement_gen = RandGenT(measurement_seed);
	measurement_dist = MeasureDistT(0, params.measurement_sigma);

	boost::random::seed_seq resample_seed{ r(), r(), r(), r(), r(), r(), r(), r() };
	resample_gen = RandGenT(resample_seed);
	resample_dist = ResampleDistT(0, 1);
}

template <class AM, class SSM>
void PF<AM, SSM >::initialize(const cv::Mat &corners){

	ssm->initialize(corners);

	if(params.pix_sigma <= 0){
		ssm->initializeSampler(state_sigma);
	} else{
		ssm->initializeSampler(params.pix_sigma);
	}

	state_sigma = ssm->getSamplerSigma();
	VectorXd state_mean = ssm->getSamplerMean();
	utils::printMatrix(state_sigma.transpose(), "state_sigma");
	//utils::printMatrix(state_mean, "state_mean");

	//utils::printMatrixToFile(state_sigma.transpose(), "state_sigma", log_fname, "%15.9f", "w");


	am->initializePixVals(ssm->getPts());
	am->initialize();
	max_similarity = am->getSimilarity();
	utils::printScalar(max_similarity, "max_similarity");

	initializeParticles();

	prev_corners = ssm->getCorners();
	ssm->getCorners(cv_corners_mat);
}

template <class AM, class SSM>
void PF<AM, SSM >::initializeParticles(){
	double init_wt = 1.0 / params.n_particles;
	for(int particle_id = 0; particle_id < params.n_particles; particle_id++){
		particle_states[curr_set_id][particle_id] = ssm->getState();
		particle_wts[particle_id] = init_wt;
		if(particle_id > 0){
			particle_cum_wts[particle_id] = particle_wts[particle_id] + particle_cum_wts[particle_id - 1];
		} else{
			particle_cum_wts[particle_id] = particle_wts[particle_id];
		}
		particle_ar[curr_set_id][particle_id].setZero();
	}
}

template <class AM, class SSM>
void PF<AM, SSM >::update(){
	++frame_id;
	am->setFirstIter();
	for(int i = 0; i < params.max_iters; i++){
		mean_corners.setZero();
		for(int particle_id = 0; particle_id < params.n_particles; particle_id++){
			switch(params.dyn_model){
			case DynamicModel::AutoRegression1:
				switch(params.upd_type){
				case UpdateType::Additive:
					ssm->additiveAutoRegression1(perturbed_state, perturbed_ar,
						particle_states[curr_set_id][particle_id], particle_ar[curr_set_id][particle_id]);
					break;
				case UpdateType::Compositional:
					ssm->compositionalAutoRegression1(perturbed_state, perturbed_ar,
						particle_states[curr_set_id][particle_id], particle_ar[curr_set_id][particle_id]);
					break;
				}
				particle_ar[curr_set_id][particle_id] = perturbed_ar;
				break;
			case DynamicModel::RandomWalk:
				switch(params.upd_type){
				case UpdateType::Additive:
					ssm->additiveRandomWalk(perturbed_state, particle_states[curr_set_id][particle_id]);
					break;
				case UpdateType::Compositional:
					ssm->compositionalRandomWalk(perturbed_state, particle_states[curr_set_id][particle_id]);
					break;
				}
				break;
			}
			particle_states[curr_set_id][particle_id] = perturbed_state;		

			ssm->setState(particle_states[curr_set_id][particle_id]);
			am->updatePixVals(ssm->getPts());
			am->update(false);

			// a positive number that measures the dissimilarity between the
			// template and the patch corresponding to this particle
			double measuremnt_val = max_similarity - am->getSimilarity(); 

			// convert this dissimilarity to a likelihood proportional to the dissimilarity
			switch(params.likelihood_func){
			case LikelihoodFunc::AM:
				measurement_likelihood = am->getLikelihood();
				break;
			case LikelihoodFunc::Gaussian:
				measurement_likelihood = measurement_factor * exp(-0.5*measuremnt_val / params.measurement_sigma);
				break;
			case LikelihoodFunc::Reciprocal:
				measurement_likelihood = 1.0 / (1.0 + measuremnt_val);
				break;
			}

			if(params.debug_mode){
				utils::printMatrix(ssm->getCorners(), "corners");
				utils::printScalar(measuremnt_val, "measuremnt_val");
				utils::printScalar(measurement_likelihood, "measurement_likelihood");
			}

			particle_wts[particle_id] = measurement_likelihood;
			if(particle_id > 0){
				particle_cum_wts[particle_id] = particle_wts[particle_id] + particle_cum_wts[particle_id - 1];
			} else{
				particle_cum_wts[particle_id] = particle_wts[particle_id];
			}
		}
		if(params.debug_mode){
			utils::printMatrix(particle_wts.transpose(), "particle_wts");
			utils::printMatrix(particle_cum_wts.transpose(), "particle_cum_wts");
		}
		switch(params.resampling_type){
		case ResamplingType::None:
			break;
		case ResamplingType::BinaryMultinomial:
			binaryMultinomialResampling();
			break;
		case ResamplingType::LinearMultinomial:
			linearMultinomialResampling();
			break;
		case ResamplingType::Residual:
			residualResampling();
			break;
		}
		if(params.mean_of_corners){
			ssm->setCorners(mean_corners);
		} else{
			ssm->estimateMeanOfSamples(mean_state, particle_states[curr_set_id], params.n_particles);
			ssm->setState(mean_state);
		}

		double update_norm = (prev_corners - ssm->getCorners()).squaredNorm();
		prev_corners = ssm->getCorners();
		if(update_norm < params.epsilon){
			if(params.debug_mode){
				printf("n_iters: %d\n", i + 1);
			}
			break;
		}
		am->clearFirstIter();
	}
	if(params.reset_to_mean){
		initializeParticles();
	}
	ssm->getCorners(cv_corners_mat);
}

// uses binary search to find the particle with the smallest
// index whose cumulative weight is greater than the provided
// random number supposedly drawn from a uniform distribution 
// between 0 and max cumulative weight
template <class AM, class SSM>
void PF<AM, SSM >::binaryMultinomialResampling(){
	// change the range of the uniform distribution used for resampling instead of normalizing the weights
	//resample_dist.param(ResampleDistParamT(0, particle_cum_wts[params.n_particles - 1]));

	// normalize the cumulative weights and leave the uniform distribution range to (0, 1]
	particle_cum_wts /= particle_cum_wts[params.n_particles - 1];
	if(params.debug_mode){
		utils::printMatrix(particle_cum_wts.transpose(), "normalized particle_cum_wts");
	}

	for(int particle_id = 0; particle_id < params.n_particles; ++particle_id){
		double uniform_rand_num = resample_dist(resample_gen);
		int lower_id = 0, upper_id = params.n_particles - 1;
		int resample_id = (lower_id + upper_id) / 2;
		while(upper_id > lower_id){
			if(particle_cum_wts[resample_id] >= uniform_rand_num){
				upper_id = resample_id;
			} else{
				lower_id = resample_id + 1;
			}
			resample_id = (lower_id + upper_id) / 2;
		}

		if(params.debug_mode){
			utils::printScalar(uniform_rand_num, "uniform_rand_num");
			utils::printScalar(resample_id, "resample_id", "%d");
		}

		// place the resampled particle states into the other set so as not to overwrite the current one
		particle_states[1 - curr_set_id][particle_id] = particle_states[curr_set_id][resample_id];
		particle_ar[1 - curr_set_id][particle_id] = particle_ar[curr_set_id][resample_id];
		if(params.mean_of_corners){
			updateMeanCorners(particle_states[curr_set_id][resample_id], particle_id);
		}
	}
	// make the other particle set the current one
	curr_set_id = 1 - curr_set_id;
}

template <class AM, class SSM>
void PF<AM, SSM >::linearMultinomialResampling(){
	// change the range of the uniform distribution used for resampling instead of normalizing the weights
	//resample_dist.param(ResampleDistParamT(0, particle_cum_wts[params.n_particles - 1]));

	// normalize the cumulative weights and leave the uniform distribution range to (0, 1]
	particle_cum_wts /= particle_cum_wts[params.n_particles - 1];
	if(params.debug_mode){
		utils::printMatrix(particle_cum_wts.transpose(), "normalized particle_cum_wts");
	}

	for(int particle_id = 0; particle_id < params.n_particles; ++particle_id){
		double uniform_rand_num = resample_dist(resample_gen);
		int resample_id = 0;
		while(particle_cum_wts[resample_id] < uniform_rand_num){ ++resample_id; }

		if(params.debug_mode){
			utils::printScalar(uniform_rand_num, "uniform_rand_num");
			utils::printScalar(resample_id, "resample_id", "%d");
		}

		// place the resampled particle states into the other set so as not to overwrite the current one
		particle_states[1 - curr_set_id][particle_id] = particle_states[curr_set_id][resample_id];
		particle_ar[1 - curr_set_id][particle_id] = particle_ar[curr_set_id][resample_id];
		if(params.mean_of_corners){
			updateMeanCorners(particle_states[curr_set_id][resample_id], particle_id);
		}
	}
	// make the other particle set the current one
	curr_set_id = 1 - curr_set_id;
}



template <class AM, class SSM>
void PF<AM, SSM >::residualResampling() {
	// normalize the weights
	particle_wts /= particle_cum_wts[params.n_particles - 1];
	// vector of particle indies
	VectorXi particle_idx = VectorXi::LinSpaced(params.n_particles, 0, params.n_particles - 1);
	if(params.debug_mode){
		utils::printMatrix(particle_wts.transpose(), "normalized particle_wts");
		utils::printMatrix(particle_idx.transpose(), "particle_idx", "%d");
	}
	// sort, with highest weight first
	std::sort(particle_idx.data(), particle_idx.data() + params.n_particles - 1,
		[&](int a, int b){
		return particle_wts[a] > particle_wts[b];
	});
	if(params.debug_mode){
		utils::printMatrix(particle_idx.transpose(), "sorted particle_idx", "%d");
	}

	// now we append	
	int particles_found = 0;
	for(int particle_id = 0; particle_id < params.n_particles; ++particle_id) {
		int resample_id = particle_idx[particle_id];
		int particle_copies = round(particle_wts[resample_id] * params.n_particles);
		for(int copy_id = 0; copy_id < particle_copies; ++copy_id) {
			particle_states[1 - curr_set_id][particles_found] = particle_states[curr_set_id][resample_id];
			particle_ar[1 - curr_set_id][particles_found] = particle_ar[curr_set_id][resample_id];
			if(params.mean_of_corners){
				updateMeanCorners(particle_states[curr_set_id][resample_id], particles_found);
			}
			if(++particles_found == params.n_particles) { break; }
		}
		if(particles_found == params.n_particles) { break; }
	}
	int resample_id = particle_idx[0];
	for(int particle_id = particles_found; particle_id < params.n_particles; ++particle_id) {
		// duplicate particle with highest weight to get exactly same number again
		particle_states[1 - curr_set_id][particle_id] = particle_states[curr_set_id][resample_id];
		particle_ar[1 - curr_set_id][particle_id] = particle_ar[curr_set_id][resample_id];
		if(params.mean_of_corners){
			updateMeanCorners(particle_states[curr_set_id][resample_id], particle_id);
		}
	}
	curr_set_id = 1 - curr_set_id;
}

const char* PFParams::toString(DynamicModel _dyn_model){
	switch(_dyn_model){
	case DynamicModel::RandomWalk:
		return "RandomWalk";
	case DynamicModel::AutoRegression1:
		return "AutoRegression1";
	default:
		throw std::invalid_argument("Invalid dynamic model provided");
	}
}
const char* PFParams::toString(UpdateType _upd_type){
	switch(_upd_type){
	case UpdateType::Additive:
		return "Additive";
	case UpdateType::Compositional:
		return "Compositional";
	default:
		throw std::invalid_argument("Invalid dynamic model provided");
	}
}
const char* PFParams::toString(ResamplingType _resampling_type){
	switch(_resampling_type){
	case ResamplingType::None:
		return "None";
	case ResamplingType::BinaryMultinomial:
		return "BinaryMultinomial";
	case ResamplingType::LinearMultinomial:
		return "LinearMultinomial";
	case ResamplingType::Residual:
		return "Residual";
	default:
		throw std::invalid_argument("Invalid resampling type provided");
	}
}
const char* PFParams::toString(LikelihoodFunc _likelihood_func){
	switch(_likelihood_func){
	case LikelihoodFunc::AM:
		return "AM";
	case LikelihoodFunc::Gaussian:
		return "Gaussian";
	case LikelihoodFunc::Reciprocal:
		return "Reciprocal";
	default:
		throw std::invalid_argument("Invalid likelihood function provided");
	}
}
PFParams::PFParams(int _max_iters, int _n_particles, double _epsilon,
	DynamicModel _dyn_model, UpdateType _upd_type,
	LikelihoodFunc _likelihood_func,
	ResamplingType _resampling_type,
	bool _reset_to_mean, bool _mean_of_corners,
	const vector<double> &_ssm_sigma,
	double _pix_sigma, double _measurement_sigma,
	bool _debug_mode){
	max_iters = _max_iters;
	n_particles = _n_particles;
	epsilon = _epsilon;
	dyn_model = _dyn_model;
	upd_type = _upd_type;
	likelihood_func = _likelihood_func;
	resampling_type = _resampling_type;
	reset_to_mean = _reset_to_mean;
	mean_of_corners = _mean_of_corners;
	ssm_sigma = _ssm_sigma;
	pix_sigma = _pix_sigma;
	measurement_sigma = _measurement_sigma;
	debug_mode = _debug_mode;
}

PFParams::PFParams(PFParams *params) :
max_iters(PF_MAX_ITERS),
n_particles(PF_N_PARTICLES),
epsilon(PF_EPSILON),
dyn_model(static_cast<DynamicModel>(PF_DYN_MODEL)),
upd_type(static_cast<UpdateType>(PF_UPD_TYPE)),
likelihood_func(static_cast<LikelihoodFunc>(PF_LIKELIHOOD_FUNC)),
resampling_type(static_cast<ResamplingType>(PF_RESAMPLING_TYPE)),
reset_to_mean(PF_RESET_TO_MEAN),
mean_of_corners(PF_MEAN_OF_CORNERS),
pix_sigma(PF_PIX_SIGMA),
measurement_sigma(PF_MEASUREMENT_SIGMA),
debug_mode(PF_DEBUG_MODE){
	if(params){
		max_iters = params->max_iters;
		n_particles = params->n_particles;
		epsilon = params->epsilon;
		dyn_model = params->dyn_model;
		upd_type = params->upd_type;
		likelihood_func = params->likelihood_func;
		resampling_type = params->resampling_type;
		reset_to_mean = params->reset_to_mean;
		mean_of_corners = params->mean_of_corners;
		ssm_sigma = params->ssm_sigma;
		pix_sigma = params->pix_sigma;
		measurement_sigma = params->measurement_sigma;
		debug_mode = params->debug_mode;
	}
}


_MTF_END_NAMESPACE

#include "mtf/Macros/register.h"
_REGISTER_TRACKERS(PF);
