#ifndef NN_H
#define NN_H

#include "SearchMethod.h"
#include <flann/flann.hpp>

#define NN_MAX_ITERS 10
#define NN_EPSILON 0.01
#define NN_CORNER_SIGMA_D 0.06
#define NN_CORNER_SIGMA_T 0.04
#define NN_PIX_SIGMA 0
#define NN_N_SAMPLES 1000
#define NN_N_TREES 6
#define NN_N_CHECKS 50
#define NN_INDEX_TYPE 1
#define NN_CONFIG_FILE "Config/nn.cfg"
#define NN_ADDITIVE_UPDATE 1
#define NN_DIRECT_SAMPLES 1
#define NN_SSM_SIGMA_PREC 1.1
#define NN_LOAD_INDEX 0
#define NN_SAVE_INDEX 0
#define NN_INDEX_FILE_TEMPLATE "nn_saved_index"
#define NN_DEBUG_MODE false
#define NN_CORNER_GRAD_EPS 1e-5

#define NN_KDT_TREES 6
#define NN_KM_BRANCHING 32
#define NN_KM_ITERATIONS 11
#define NN_KM_CENTERS_INIT flann::FLANN_CENTERS_RANDOM
#define NN_KM_CB_INDEX 0.2
#define NN_KDTS_LEAF_MAX_SIZE 10
#define NN_KDTC_LEAF_MAX_SIZE 64
#define NN_HC_BRANCHING 32
#define NN_HC_CENTERS_INIT flann::FLANN_CENTERS_RANDOM
#define NN_HC_TREES 4
#define NN_HC_LEAF_MAX_SIZE 100
#define NN_AUTO_TARGET_PRECISION 0.9
#define NN_AUTO_BUILD_WEIGHT 0.01
#define NN_AUTO_MEMORY_WEIGHT 0
#define NN_AUTO_SAMPLE_FRACTION 0.1

_MTF_BEGIN_NAMESPACE

// index specific params
struct NNIndexParams{
	int kdt_trees;
	int km_branching;
	int km_iterations;
	flann::flann_centers_init_t km_centers_init;
	float km_cb_index;
	int kdts_leaf_max_size;
	int kdtc_leaf_max_size;
	int hc_branching;
	flann::flann_centers_init_t hc_centers_init;
	int hc_trees;
	int hc_leaf_max_size;
	float auto_target_precision;
	float auto_build_weight;
	float auto_memory_weight;
	float auto_sample_fraction;

	NNIndexParams(int kdt_trees,
		int km_branching,
		int km_iterations,
		flann::flann_centers_init_t,
		float km_cb_index,
		int kdts_leaf_max_size,
		int kdtc_leaf_max_size,
		int hc_branching,
		flann::flann_centers_init_t,
		int hc_trees,
		int hc_leaf_max_size,
		float auto_target_precision,
		float auto_build_weight,
		float auto_memory_weight,
		float auto_sample_fraction);
	NNIndexParams(NNIndexParams *params = nullptr);
};
struct NNParams{
	enum class IdxType{
		KDTree, HierarchicalClustering, KMeans, Composite, Linear,
		KDTreeSingle, KDTreeCuda3d, Autotuned
	};
	static const char* toString(IdxType index_type);

	int max_iters; //! maximum iterations of the NN algorithm to run for each frame
	int n_samples;
	double epsilon; //! maximum L2 norm of the state update vector at which to stop the iterations	
	
	vector<double> ssm_sigma;
	double corner_sigma_d;
	double corner_sigma_t;
	double pix_sigma;

	IdxType index_type;
	int n_checks;

	bool additive_update;
	bool direct_samples;

	bool save_index;
	bool load_index;
	string saved_index_dir;

	double ssm_sigma_prec; //! precision within which the change in corners produced by the change in
	// a parameter for the chosen sigma should match the desired change in corners
	bool debug_mode; //! decides whether logging data will be printed for debugging purposes; 
	//! only matters if logging is enabled at compile time
	NNParams(int _max_iters, int _n_samples, double _epsilon,
		const vector<double> &_ssm_sigma,
		double _corner_sigma_d, double _corner_sigma_t,
		double _pix_sigma, IdxType _index_type, int _n_checks,
		bool _additive_update, bool _direct_samples,
		bool load_index, bool _save_index, string _saved_index_dir,
		double _ssm_sigma_prec,	bool _debug_mode);
	NNParams(NNParams *params = nullptr);
};

template<class AM, class SSM>
class NN : public SearchMethod < AM, SSM > {
	init_profiling();
	char *log_fname;
	char *time_fname;

	//PixGradT pix_grad_identity;
	//MatrixXd ssm_grad_norm;
	//RowVectorXd ssm_grad_norm_mean;
public:
	typedef flann::Matrix<double> flannMatT;
	typedef flann::Matrix<int> flannResultT;
	typedef flann::Index<AM> flannIdxT;
	//typedef flann::Index<flann::L2<double> > flannIdxT;

	typedef NNParams ParamType;
	ParamType params;
	typedef ParamType::IdxType IdxType;

	typedef NNIndexParams IdxParamType;
	IdxParamType idx_params;

	using SearchMethod<AM, SSM> ::am;
	using SearchMethod<AM, SSM> ::ssm;
	using typename SearchMethod<AM, SSM> ::AMParams;
	using typename SearchMethod<AM, SSM> ::SSMParams;
	using SearchMethod<AM, SSM> ::cv_corners_mat;
	using SearchMethod<AM, SSM> ::name;
	using SearchMethod<AM, SSM> ::initialize;
	using SearchMethod<AM, SSM> ::update;

	int frame_id;
	int am_dist_size;
	int ssm_state_size;

	Matrix3d warp_update;
	VectorXd state_sigma;

	Matrix24d prev_corners;
	VectorXd ssm_update;	
	VectorXd inv_state_update;
	
	MatrixXd ssm_perturbations;

	MatrixXdr eig_dataset;
	VectorXd eig_query;
	VectorXi eig_result;
	VectorXd eig_dists;

	flannMatT *flann_dataset;
	flannMatT *flann_query;
	flannIdxT *flann_index;
	//flannResultT *flann_result;
	//flannMatT *flann_dists;

	int best_idx;
	double best_dist;

	string saved_db_path, saved_idx_path;

	NN(ParamType *nn_params = nullptr, IdxParamType *nn_idx_params = nullptr,
		AMParams *am_params = nullptr, SSMParams *ssm_params = nullptr);
	~NN(){
		delete(flann_dataset);
		delete(flann_query);
		delete(flann_index);
		//delete(flann_result);
		//delete(flann_dists);
	}

	void initialize(const cv::Mat &corners) override;
	void update() override;
	const flann::IndexParams getIndexParams();

	//double getCornerChangeNorm(const VectorXd &state_update);
	//double findSSMSigma(int param_id);
	//double findSSMSigmaGrad(int param_id);
};

_MTF_END_NAMESPACE

#endif

