#ifndef ICLK_H
#define ICLK_H

#include "SearchMethod.h"

#define ICLK_MAX_ITERS 10
#define ICLK_EPSILON 0.01
#define ICLK_HESS_TYPE 0
#define ICLK_SEC_ORD_HESS false
#define ICLK_UPDATE_SSM false
#define ICLK_CHAINED_WARP false
#define ICLK_DEBUG_MODE false

_MTF_BEGIN_NAMESPACE

struct ICLKParams{
	enum class HessType{ InitialSelf, CurrentSelf, Std };
	int max_iters; //! maximum iterations of the ICLK algorithm to run for each frame
	double epsilon; //! maximum L2 norm of the state update vector at which to stop the iterations
	HessType hess_type;
	bool sec_ord_hess;
	bool update_ssm;
	bool chained_warp;
	bool debug_mode; //! decides whether logging data will be printed for debugging purposes; 
	//! only matters if logging is enabled at compile time

	ICLKParams(int _max_iters, double _epsilon,
		HessType _hess_type, bool _sec_ord_hess,
		bool _update_ssm, bool _chained_warp,
		bool _debug_mode);
	ICLKParams(ICLKParams *params = nullptr);
};

template<class AM, class SSM>
class ICLK : public SearchMethod < AM, SSM > {

private:
	init_profiling();
	char *log_fname;
	char *time_fname;

public:
	typedef ICLKParams ParamType;
	typedef typename ParamType::HessType HessType;
	ParamType params;

	using SearchMethod<AM, SSM> ::am;
	using SearchMethod<AM, SSM> ::ssm;
	using typename SearchMethod<AM, SSM> ::AMParams;
	using typename SearchMethod<AM, SSM> ::SSMParams;
	using SearchMethod<AM, SSM> ::cv_corners_mat;
	using SearchMethod<AM, SSM> ::name;

	using SearchMethod<AM, SSM> ::initialize;
	using SearchMethod<AM, SSM> ::update;

	// Let S = size of SSM state vector and N = resx * resy = no. of pixels in the object patch
	//! 1 x S Jacobian of the AM error norm w.r.t. SSM state vector
	RowVectorXd jacobian;
	//! S x S Hessian of the AM error norm w.r.t. SSM state vector
	MatrixXd hessian;
	//! N x S jacobians of the pixel values w.r.t the SSM state vector 
	MatrixXd init_pix_jacobian, curr_pix_jacobian;
	//! N x S x S hessians of the pixel values w.r.t the SSM state vector stored as a (S*S) x N 2D matrix
	MatrixXd init_pix_hessian, curr_pix_hessian;

	Matrix24d prev_corners;
	VectorXd ssm_update, inv_update;
	int frame_id;

	ICLK(ParamType *iclk_params = nullptr,
		AMParams *am_params = nullptr, SSMParams *ssm_params = nullptr);

	void initialize(const cv::Mat &corners) override;
	void update() override;
	void setRegion(const cv::Mat& corners) override;
};
_MTF_END_NAMESPACE

#endif

