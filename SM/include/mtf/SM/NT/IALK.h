#ifndef IALK_NT_H
#define IALK_NT_H

#include "SearchMethod.h"

#define IALK_MAX_ITERS 10
#define IALK_EPSILON 0.01
#define IALK_HESS_TYPE 0
#define IALK_SEC_ORD_HESS false
#define IALK_DEBUG_MODE false

_MTF_BEGIN_NAMESPACE
namespace nt{

	struct IALKParams{
		enum class HessType{ InitialSelf, CurrentSelf, Std };
		int max_iters; //! maximum iterations of the IALK algorithm to run for each frame
		double epsilon; //! maximum L1 norm of the state update vector at which to stop the iterations
		HessType hess_type;
		bool sec_ord_hess;
		bool debug_mode; //! decides whether logging data will be printed for debugging purposes; 
		//! only matters if logging is enabled at compile time

		IALKParams(int _max_iters, double _epsilon,
			HessType _hess_type, bool _sec_ord_hess,
			bool _debug_mode);
		IALKParams(IALKParams *params = nullptr);
	};

	class IALK : public SearchMethod{

		init_profiling();
		char *log_fname;
		char *time_fname;

	public:

		typedef IALKParams ParamType;
		typedef typename ParamType::HessType HessType;

		ParamType params;

		using SearchMethod::initialize;
		using SearchMethod::update;

		// Let S = size of SSM state vector and N = resx * resy = no. of pixels in the object patch

		//! 1 x S Jacobian of the AM error norm w.r.t. SSM state vector
		RowVectorXd jacobian;
		//! S x S Hessian of the AM error norm w.r.t. SSM state vector
		MatrixXd hessian;
		//! N x S jacobians of the pix values w.r.t the SSM state vector where N = resx * resy
		//! is the no. of pixels in the object patch
		//! N x S jacobians of the pix values w.r.t the SSM state vector 
		MatrixXd init_pix_jacobian, curr_pix_jacobian;
		//! N x S x S hessians of the pixel values w.r.t the SSM state vector stored as a (S*S) x N 2D matrix
		MatrixXd init_pix_hessian, curr_pix_hessian;

		Matrix24d prev_corners;
		VectorXd ssm_update;
		Matrix3d warp_update;
		int frame_id;

		IALK(AppearanceModel *_am, StateSpaceModel *_ssm,
			ParamType *fclk_params = nullptr);

		void initialize(const cv::Mat &corners) override;
		void update() override;
	};
}
_MTF_END_NAMESPACE

#endif

