#ifndef FCLK_NT_H
#define FCLK_NT_H

#include "SearchMethod.h"

#define FCLK_MAX_ITERS 10
#define FCLK_EPSILON 0.01
#define FCLK_HESS_TYPE 0
#define FCLK_SECOND_ORDER_HESS false
#define FCLK_CHAINED_WARP false
#define FCLK_UPD_TEMPL false
#define FCLK_DEBUG_MODE false

_MTF_BEGIN_NAMESPACE
namespace nt{
	struct FCLKParams{
		enum class HessType{ InitialSelf, CurrentSelf, Std };

		int max_iters; //! maximum iterations of the FCLK algorithm to run for each frame
		double epsilon; //! maximum L1 norm of the state update vector at which to stop the iterations
		HessType hess_type;
		bool sec_ord_hess;
		bool upd_templ;
		bool chained_warp;
		bool debug_mode; //! decides whether logging data will be printed for debugging purposes; 
		//! only matters if logging is enabled at compile time

		FCLKParams(int _max_iters, double _epsilon,
			HessType _hess_type, bool _sec_ord_hess,
			bool _upd_templ, bool _chained_warp,
			bool _debug_mode);
		FCLKParams(FCLKParams *params = nullptr);
	};

	// Non templated implementation of FCLK
	class FCLK : public SearchMethod {
		init_profiling();
		char *log_fname;
		char *time_fname;

	public:

		typedef FCLKParams ParamType;
		ParamType params;
		typedef ParamType::HessType HessType;

		using SearchMethod::am;
		using SearchMethod::ssm;
		using SearchMethod::cv_corners_mat;
		using SearchMethod::name;
		using SearchMethod::initialize;
		using SearchMethod::update;

		// Let S = size of SSM state vector and N = resx * resy = no. of pixels in the object patch

		//! 1 x S Jacobian of the appearance model w.r.t. SSM state vector
		RowVectorXd jacobian;
		//! S x S Hessian of the appearance model w.r.t. SSM state vector
		MatrixXd hessian;
		//! N x S jacobians of the pix values w.r.t the SSM state vector 
		MatrixXd init_pix_jacobian, curr_pix_jacobian;
		MatrixXd curr_pix_jacobian_new;

		//! N x S x S hessians of the pixel values w.r.t the SSM state vector stored as a (S*S) x N 2D matrix
		MatrixXd init_pix_hessian, curr_pix_hessian;

		Matrix24d prev_corners;
		VectorXd ssm_update;
		int frame_id;

		FCLK(AppearanceModel *_am, StateSpaceModel *_ssm,
			ParamType *fclk_params = nullptr);

		void initialize(const cv::Mat &corners) override;
		void update() override;
		void setRegion(const cv::Mat& corners) override;
	};
}



_MTF_END_NAMESPACE

#endif

