
#ifndef BUILD_GRAPH_H
#define BUILD_GRAPH_H

#include "utility.h"
namespace gnn{
	struct node{
		int *nns_inds;
		int size;
		int capacity;
	};

	//extern long *X;
	//extern int NUM_ROWS;  // number of nodes
	void add_node(struct node* nod, int ind);
	template<typename DistType>
	struct node* build_graph(double *X, int k, int n_samples, int n_dims, const DistType &dist_func)
	{
		struct node *Nodes = static_cast<node*>(malloc(n_samples*sizeof(struct node)));
		for(int i = 0; i < n_samples; i++)
		{
			Nodes[i].nns_inds = static_cast<int*>(malloc(k*sizeof(int)));
			check_pointer(Nodes[i].nns_inds, "Couldn't malloc node");
			//   memset(Nodes[i].nns_inds, -1, k*sizeof(int));
			Nodes[i].capacity = k;
			Nodes[i].size = 0;
		}

		//  struct indx_dist *dists = malloc(n_samples * sizeof(struct indx_dist));
		//  check_pointer(dists, "Couldn't malloc dists");
		struct indx_dist* dists = static_cast<indx_dist*>(malloc((k + 1) * sizeof(struct indx_dist)));
		check_pointer(dists, "Couldn't malloc dists");

		int nns_ind;
		//  int *nns_ind = malloc(k*sizeof(int));
		//  check_pointer(nns_ind, "Couldn't malloc nns_ind");
		double *query;
		//  int *query = malloc(n_dims*sizeof(int));
		//  check_pointer(query, "Couldn't malloc query");

		for(int i = 0; i < n_samples; i++){
			query = X + (i*n_dims);
			knn_search2<DistType>(query, dists, X, n_samples, n_dims, k + 1, dist_func);   // index of 1st node is 0
			for(int j = 0; j < k; j++){
				nns_ind = dists[j + 1].idx;
				add_node(&Nodes[i], nns_ind);
			}
		}
		free(dists);
		return Nodes;
	}
}
#endif
